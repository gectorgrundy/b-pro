<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'line1' => array(
        'caption' => '% скидки',
        'type' => 'number'
    ),
    'line2' => array(
        'caption' => 'Заказ от',
        'type' => 'text'
	)
	,
    'line3' => array(
        'caption' => 'скидка в грн',
        'type' => 'number'
	)

);
$settings['templates'] = array(
    'outerTpl' => '<div class="images">[+wrapper+]</div>',
    'rowTpl' => '<div class="image">[+imagebg+][+line1+][+line2+]</div>'
);
