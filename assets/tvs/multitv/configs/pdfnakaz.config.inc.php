<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
     'file' => array(
        'caption' => 'Документ к приказу',
        'type' => 'file'
    ),
    'title' => array(
        'caption' => 'Название(если пусто, то Аннотация)',
        'type' => 'text'
    )
);
$settings['templates'] = array(
    'outerTpl' => '<ul>[+wrapper+]</ul>',
    'rowTpl' => '<li>[+text+], [+image+], [+thumb+], [+textarea+], [+date+], [+dropdown+], [+listbox+], [+listbox-multiple+], [+checkbox+], [+option+]</li>'
);
$settings['configuration'] = array(
    'enablePaste' => true,
    'enableClear' => true,
    'csvseparator' => ','
);
