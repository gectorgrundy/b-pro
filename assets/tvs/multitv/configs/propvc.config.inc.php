<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
    'image1_1' => array(
        'caption' => 'Іконка',
        'type' => 'image'
    ),
	'title1' => array(
        'caption' => 'Заголовок',
        'type' => 'text'
    ),
    'title2' => array(
        'caption' => 'Опис',
        'type' => 'textareamini'
    )
);
$settings['templates'] = array(
    'outerTpl' => '<div class="flex-container"><div class="flexslider"><ul class="slides">[+wrapper+]</ul></div></div>',
    'rowTpl' => '<li>[+link:ne=``:then=`<a href="[+link+]"><img src="[+image+]" alt="[+title+]" /></a>`:else=`<img src="[+image+]" alt="[+title+]" />`+]<div class="flex-caption"><h2>[+title+]</h2><p>[+legend+]</p></div></li>'
);