

var langTxt = new Array();

langTxt['delAll'] = "Очистити кошик";
langTxt['select'] = "Ви вибрали:";
langTxt['empty'] = "Пусто";
langTxt['confirm'] = "Ви впевнені?";
langTxt['continue'] = "ОК"; //&radic;
langTxt['yes'] = "Да";
langTxt['cancel'] = "Відміна"; //&times;
langTxt['cookieError'] = "Для нормальної роботи в браузері повинні бути включені cookies.";
langTxt['delete'] = "Видалити";
langTxt['delGoods'] = "Видалити товар";
langTxt['goods'] = "товар";
langTxt['count'] = "Кіл-ть";
langTxt['sumTotal'] = "Загальна сума:";
langTxt['executeOrder'] = "Оформити замовлення";
langTxt['changeCount'] = "Змінити кількість";
langTxt['addedToCart'] = "Товар доданий в кошик";
