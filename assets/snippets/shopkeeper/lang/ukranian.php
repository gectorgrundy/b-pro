<?php

//Языковой файл для сниппета Shopkeeper 1.0

$langTxt = array(
  "this" => "ukranian",
  "confirm" => "Видалити?",
  "noOrders" => "Заказів немає",
  "noSelected" => "Товари не обрані",
  "currency" => "Валюта",
  "currencyDefault" => "грн.",
  "sumTotal" => "Загальна вартість",
  "plural" => array("товар","товара","товарів"),
  "pluraluk" => array("товар","товара","товарів")
);


?>