<?php
		$idglres = $_GET['idcat'];
require_once('../../../manager/includes/config.inc.php');
require_once('../../../manager/includes/protect.inc.php');
define('MODX_API_MODE', true);
require_once('../../../manager/includes/document.parser.class.inc.php');
$modx = new DocumentParser;
$modx->db->connect();
$modx->getSettings();
include("../../../assets/modules/catalogFill/classes/PHPExcel.php");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
		//$active_sheet->setOddHeader('&CB-pro: прайс-лист');
        $active_sheet = $objPHPExcel->getActiveSheet();
	
        $active_sheet->getPageSetup()
            ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT); 
        $active_sheet->getPageSetup()
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4); 
$style_wrap = array(
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        ),
        'allborders'=>array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'CCCCCC'
            )
        )
    ),
    'font'=>array(
        'bold' => true,
        'italic' => true,
        'size' => 12
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
    )
);
$style_wrapt = array(
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        ),
        'allborders'=>array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'CCCCCC'
            )
        )
    )
);
$style_wrapv = array(
	'alignment' => array(
		'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
       	'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'rotation' => 0,
		'wrap' => true
    )
);
$style_pcat = array(
    'font'=>array(
        'bold' => true,
        'size' => 12,
        'color'=>array(
            'rgb' => '000000'
        )
    ),
    'fill' => array(
        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
        'color'=>array(
            'rgb' => 'B8CCE4'
        )
    ),
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        )
    )
);
$style_all = array(
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
			)
        ),
  
    'font'=>array(
        'bold' => true,
		'size' => 14,
        'color'=>array(
            'rgb' => '000000'
        )
    )
);
$style_dat = array(
    'font'=>array(
        'bold' => true,
        'size' => 12,
        'color'=>array(
            'rgb' => '000000'
        )
    )

);
$style_rozd = array(
    'font'=>array(
        'bold' => true,
        'size' => 14,
        'color'=>array(
            'rgb' => '0062FF'
        )
    )

);
$style_centr = array(
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'wrap' => true,
		'rotation' => 0
    ),
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        )
    )
);
$style_ncentr = array(
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_LEFT,
		'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'wrap' => true,
		'rotation' => 0
    ),
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        )
    )
);
$style_rcentr = array(
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
		'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'wrap' => true,
		'rotation' => 0
    ),
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        )
    )
);
$style_ntop = array(
    'alignment' => array(
       	'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'rotation' => 0
    ),
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        )
    )
);

$style_rtop = array(
    'alignment' => array(
		'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
       	'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'rotation' => 0,
		'wrap' => true
    )
);
$style_rtext = array(
    'alignment' => array(
		'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_LEFT,
       	'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'rotation' => 0,
		'wrap' => true
    ),
    'font'=>array(
        'bold' => false,
        'size' => 12,
        'color'=>array(
            'rgb' => '000000'
        )
	)
    
);
$style_cat_all = array( 
    'font'=>array(
        'bold' => true,
        'size' => 14,
        'color'=>array(
            'rgb' => '000000'
        )
    ),
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        )
    )

);
$style_pcat_all = array( 
    'font'=>array(
        'bold' => true,
		'italic' => true,
        'size' => 13,
        'color'=>array(
            'rgb' => '333333'
        )
    ),
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        )
    )

);
$style_ppcat_all = array( 
    'font'=>array(
        'bold' => true,
		'italic' => true,
        'size' => 13,
        'color'=>array(
            'rgb' => '333333'
        )
    ),
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        )
    )

);
$style_sum_all = array( 
    'font'=>array(
        'bold' => true,
        'size' => 12,
        'color'=>array(
            'rgb' => '000000'
        )
    ),
    'borders'=>array(
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'000000'
            )
        )
    )

);
$objDrawing  = new PHPExcel_Worksheet_Drawing(); 
$objDrawing->setPath('../../../assets/images/price/bprologo.png'); 
$objDrawing->setCoordinates('A1'); 
$objDrawing->setOffsetX(15);
$objDrawing->setOffsetY(0);
$objDrawing->setRotation(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
$active_sheet->getRowDimension('1')->setRowHeight(60);
$active_sheet->getRowDimension('7')->setRowHeight(60);
$active_sheet->getStyle('A7:E7')->applyFromArray($style_wrap);
 $active_sheet->setCellValueByColumnAndRow(0, 3, 'Дата '.date('d-m-Y'));
$active_sheet->getStyle('A3')->applyFromArray($style_dat);
$rozdil = $modx->db->select("id,pagetitle,alias,menutitle,template", "modx_site_content", 'id = '.$idglres.' AND published = 1  AND template != 6', "menuindex ASC");
if($modx->db->getRecordCount($rozdil) >= 1 ) {
$rozdilrow = $modx->db->getRow($rozdil);
$pos = strripos($rozdilrow['alias'], '-');
if ($pos === false) {
$name = $rozdilrow['alias']; 
} else {
$name = strstr($rozdilrow['alias'], '-', true);
}
	}
$active_sheet->setCellValueByColumnAndRow(0, 5, 'Раздел: '.$rozdilrow['pagetitle']);
$active_sheet->getStyle('A5')->applyFromArray($style_rozd);
		$active_sheet->mergeCells('A1:D1');
		$active_sheet->getStyle('E7')->getAlignment()->setWrapText(true);
		$active_sheet->getStyle('C7')->getAlignment()->setWrapText(true);
		$active_sheet->getStyle('D7')->getAlignment()->setWrapText(true);
		$active_sheet->getStyle('F7')->getAlignment()->setWrapText(true);

		$active_sheet->getStyle('B1')->getAlignment()->setWrapText(true);
$active_sheet->getStyle('E1')->getAlignment()->setWrapText(true);
        $active_sheet->setCellValue('E1', 'Телефон: '.chr(10).'(044)353-33-77');
$active_sheet->getStyle('E1')->applyFromArray($style_dat);
		$active_sheet->getStyle('E1')->applyFromArray($style_wrapv);
//$active_sheet->setTitle("Прайс");
		$active_sheet->getColumnDimension('A')->setWidth(7);
		$active_sheet->getColumnDimension('B')->setWidth(40);
        $active_sheet->getColumnDimension('C')->setWidth(9);
        $active_sheet->getColumnDimension('D')->setWidth(15);
		$active_sheet->getColumnDimension('E')->setWidth(15);
       

         $active_sheet->setCellValue('A7', '№ п/п');
		$active_sheet->setCellValue('B7', 'Название');
        $active_sheet->setCellValue('C7', 'Кол-во'.chr(10).'шт');
        $active_sheet->setCellValue('D7', 'Цена'.chr(10).'(грн)');
		$active_sheet->setCellValue('E7', 'Сумма '.chr(10).'(грн)');
$in=7;
$category = $modx->db->select("id,pagetitle,alias,menutitle,template", "modx_site_content", 'id = '.$idglres.' AND published = 1  AND template != 6', "menuindex ASC");
if($modx->db->getRecordCount($category) >= 1 ) {

    while($categoryrow = $modx->db->getRow($category)) {

//$nextc = 5 + $in+1;
		//$active_sheet->setCellValueByColumnAndRow(0, $in, $categoryrow['pagetitle']);
		//$active_sheet->mergeCells('A'.($in).':E'.($in));
		//$active_sheet->getStyle('A'.($in).':E'.($in))->applyFromArray($style_cat_all);
	if($categoryrow['template'] == '11') {
	
$pcategory = $modx->db->select("id,pagetitle,alias,menutitle,template", "modx_site_content", 'parent = '.$categoryrow['id'].' AND published = 1  AND template != 6', "menuindex ASC");
if($modx->db->getRecordCount($pcategory) >= 1 ) {
	$in1=0;
    while($pcategoryrow = $modx->db->getRow($pcategory)) {
		$in++;
$in1++;
		$nextpc = $in+1;
		$active_sheet->setCellValueByColumnAndRow(0, $in, $pcategoryrow['pagetitle']);
		$active_sheet->mergeCells('A'.($in).':E'.($in));
		$active_sheet->getStyle('A'.($in).':E'.($in))->applyFromArray($style_pcat_all);
		if($pcategoryrow['template'] == '11') {
$ppcategory = $modx->db->select("id,pagetitle,alias,menutitle,template", "modx_site_content", 'parent = '.$pcategoryrow['id'].' AND published = 1 AND template != 6', "menuindex ASC");
if($modx->db->getRecordCount($ppcategory) >= 1 ) {
	$in2=0;
    while($ppcategoryrow = $modx->db->getRow($ppcategory)) {
		$in++;
$in1++;
		$in2++;
		$active_sheet->setCellValueByColumnAndRow(0, $in, $ppcategoryrow['pagetitle']);
		$active_sheet->mergeCells('A'.($in).':E'.($in));
		$active_sheet->getStyle('A'.($in).':E'.($in))->applyFromArray($style_ppcat_all);
		if($ppcategoryrow['template'] == '11') {
$pppcategory = $modx->db->select("id,pagetitle,alias,menutitle,template", "modx_site_content", 'parent = '.$ppcategoryrow['id'].' AND published = 1 AND template != 6', "menuindex ASC");
if($modx->db->getRecordCount($pppcategory) >= 1 ) { 
	$in3=0;
    while($pppcategoryrow = $modx->db->getRow($pppcategory)) {
		$in++;
$in1++;
		$in2++;
		$in3++;
		$active_sheet->setCellValueByColumnAndRow(0, $in, $pppcategoryrow['pagetitle']);
		$active_sheet->mergeCells('A'.($in).':E'.($in));
		$active_sheet->getStyle('A'.($in).':E'.($in))->applyFromArray($style_ppcat_all);
	}
}//
		
		} 		elseif ($ppcategoryrow['template'] == '5' OR $ppcategoryrow['template'] == '18') {
$docscat2 = $ppcategoryrow['id'];
$rezultc2 = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='30'  AND value LIKE '%".$docscat2."%'");
		
if($modx->db->getRecordCount($rezultc2) >= 1 ) {
while ($rowc2 = $modx->db->getRow($rezultc2)) {
	$search2 = stristr($rowc2['value'], '||');
		if(!empty($search2)) {
			$arr2 = explode("||", $rowc2['value']);
		$i2=0;
		foreach($arr2 as $val2) {
		$i2++;
		if($val2==$docscat2) {
				$idtt2 = $rowc2['contentid'];
				} 
			}
		} else { 
			if($rowc2['value']==$docscat2) {
				$idtt2 = $rowc2['contentid'];
			} 
		}
	$idtvv2[] = $idtt2;
	}
	$doptov2 = implode(",", $idtvv2);
$scidmsc2 = substr($doptov2, 0, -1);
$scidmsc2 = trim($scidmsc2);
}
		if(!empty($idtvv2)){
$tovardop2 = $modx->db->select("id", "modx_site_content", 'parent = '.$docscat2.' AND published = 1', "menuindex ASC");
		if($modx->db->getRecordCount($tovardop2) >= 1 ) {
    while($tovarr2 = $modx->db->getRow($tovardop2)) {
		$arrtov2[] =  $tovarr2['id'];
	}
			$tov_array2 = array_diff($arrtov2, array(''));
			$tov_line2 = implode(",", $tov_array2);
			if(!empty($tov_line2)) { $zptr2 = ','; } else { $zptr2 = ''; }
			//var_dump($tov_line2.'2');
			$doptovnw3 = $scidmsc2.''.$zptr2.''.$tov_line2;
			$doptovnw31 = trim($doptovnw3,",");
$doptov3 = ' id IN ('.$doptovnw31.') AND';
$tovar2 = $modx->db->select("id,pagetitle,alias,menutitle,template,parent", "modx_site_content", $doptov3.'  published = 1 AND template = 6', "menuindex ASC");
		}
	}else {
$doptov3 = '';
$tovar2 = $modx->db->select("id,pagetitle,alias,menutitle,template,parent", "modx_site_content", 'parent = '.$docscat2.' AND published = 1 AND template = 6', "menuindex ASC");
	}	
	unset($idtvv2);				
//$tovar2 = $modx->db->select("id,pagetitle,alias,menutitle,template,parent", "modx_site_content", 'parent = '.$docscat2.' '.$doptov2.' AND published = 1', "menuindex ASC");
if($modx->db->getRecordCount($tovar2) >= 1 ) {
	$intv=0;
    while($tovarrow2 = $modx->db->getRow($tovar2)) {
		$in++;
		$in1++;
		$in2++;
		$in3++;
		$intv++;
		if($intv==1) { $int2 = $in; }
		$tovlink =  'https://b-pro.com.ua'.$modx->makeUrl($tovarrow2['id']);
		$price  = $modx->runSnippet("DocInfo", array('docid' => $tovarrow2['id'], 'field' => 'price'));
		if(empty($price) or $price == 'цену уточняйте') { $price = 0; } else { $price = $price; }
		$nametv = $tovarrow2['pagetitle'];
		$col = 1;
		$active_sheet->setCellValueByColumnAndRow(0, $in, $intv);
		$active_sheet->setCellValueByColumnAndRow(1, $in, $nametv);
		$active_sheet->getCell('B'.$in)->getHyperlink()->setUrl($tovlink);
		$active_sheet->setCellValueByColumnAndRow(2, $in, $col);
		$active_sheet->setCellValueByColumnAndRow(3, $in, $price);
		$formula = '=SUM(C'.($in).'*D'.($in).')';
		$active_sheet->setCellValueByColumnAndRow(4, $in, $formula);
		$active_sheet->getStyle('A'.($in))->applyFromArray($style_ncentr);
		$active_sheet->getStyle('B'.($in))->applyFromArray($style_ncentr);		
		$active_sheet->getStyle('C'.($in))->applyFromArray($style_centr);
		$active_sheet->getStyle('D'.($in))->applyFromArray($style_rcentr);
		$active_sheet->getStyle('E'.($in))->applyFromArray($style_rcentr);
										}
}
	$ints3 = $in;
	$in=$in+1;
	$active_sheet->setCellValueByColumnAndRow(1, $in, 'Всего по подразделу:');
	$active_sheet->getStyle('A'.($in).':E'.($in))->applyFromArray($style_sum_all);
		$formulatv = '=SUM(E'.($int2).':E'.($ints3).')';
	$active_sheet->setCellValueByColumnAndRow(4, $in, $formulatv);
	$E[]='E'.($in);	
		$Erz[]='E'.($in);	
		}
	}
	if($Erz) {
	$Erzd = join("+", $Erz);
	unset($Erz);
	$ints3 = $in;
	$in=$in+1;
	$active_sheet->setCellValueByColumnAndRow(1, $in, 'Всего по разделу:');
	$active_sheet->getStyle('A'.($in).':E'.($in))->applyFromArray($style_sum_all);
	$Erzsum = '=('.$Erzd.')';
	$active_sheet->setCellValueByColumnAndRow(4, $in, $Erzsum);
	$Err[]='E'.($in);
	} else { continue; }
}
		}
		elseif ($pcategoryrow['template'] == '5' OR $pcategoryrow['template'] == '18') {
$docscat = $pcategoryrow['id'];
$rezultc1 = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='30'  AND value LIKE '%".$docscat."%'");
		
if($modx->db->getRecordCount($rezultc1) >= 1 ) {
while ($rowc1 = $modx->db->getRow($rezultc1)) {
	$search = stristr($rowc1['value'], '||');
		if(!empty($search)) {
			$arr = explode("||", $rowc1[value]);
		$i=0;
		foreach($arr as $val) {
		$i++;
		if($val==$docscat) {
				$idtt = $rowc1['contentid'];
				} 
			}
		} else { 
			if($rowc1['value']==$docscat) {
				$idtt = $rowc1['contentid'];
			} 
		}
	$idtv[] = $idtt;
	}
$doptov = implode(",", $idtv);
$scidmsc = substr($doptov, 0, -1);
$scidmsc = trim($scidmsc);
}
	if(!empty($idtv)){
$tovardop = $modx->db->select("id", "modx_site_content", 'parent = '.$docscat.' AND published = 1', "menuindex ASC");
		if($modx->db->getRecordCount($tovardop) >= 1 ) {
    while($tovarr1 = $modx->db->getRow($tovardop)) {
		$arrtov1[] =  $tovarr1['id'];
	}
			$tov_array1 = array_diff($arrtov1, array(''));
			$tov_line1 = implode(",", $tov_array1);
if(!empty($tov_array1)) { $zptr = ','; } else { $zptr = ''; }
			$doptovnw1 = $scidmsc.''.$zptr.''.$tov_line1;
			$doptovnw11 = trim($doptovnw1,",");
$doptov1 = ' id IN ('.$doptovnw11.') AND';
$tovar = $modx->db->select("id,pagetitle,alias,menutitle,template,parent", "modx_site_content", $doptov1.'  published = 1 AND template = 6', "menuindex ASC");
	}
	} else {
$doptov1 = '';
$tovar = $modx->db->select("id,pagetitle,alias,menutitle,template,parent", "modx_site_content", 'parent = '.$docscat.' AND published = 1 AND template = 6', "menuindex ASC");
	}
unset($idtv);
if($modx->db->getRecordCount($tovar) >= 1 ) {
	$intv1=0;
    while($tovarrow = $modx->db->getRow($tovar)) {
		$in++;
		$in1++;
		$in2++;
		$in3++;
		$intv1++;
		if($intv1==1) { $int1 = $in; } 
		$tovlink1 =  'https://b-pro.com.ua'.$modx->makeUrl($tovarrow['id']);
		$price1  = $modx->runSnippet("DocInfo", array('docid' => $tovarrow['id'], 'field' => 'price'));
		if(empty($price1) or $price1 == 'цену уточняйте') { $price1 = 0; } else { $price1 = $price1; }
		$nametv = $tovarrow['pagetitle'];
		$col1 = 1;
		$active_sheet->setCellValueByColumnAndRow(0, $in, $intv1);
		$active_sheet->setCellValueByColumnAndRow(1, $in, $nametv);
		$active_sheet->getCell('B'.$in)->getHyperlink()->setUrl($tovlink1);
		$active_sheet->setCellValueByColumnAndRow(2, $in, $col1);
		$active_sheet->setCellValueByColumnAndRow(3, $in, $price1);
		$formula1 = '=SUM(C'.($in).'*D'.($in).')';
		$active_sheet->setCellValueByColumnAndRow(4, $in, $formula1);
		
		$active_sheet->getStyle('A'.($in))->applyFromArray($style_ncentr);
		$active_sheet->getStyle('B'.($in))->applyFromArray($style_ncentr);		
		$active_sheet->getStyle('C'.($in))->applyFromArray($style_centr);
		$active_sheet->getStyle('D'.($in))->applyFromArray($style_rcentr);
		$active_sheet->getStyle('E'.($in))->applyFromArray($style_rcentr);
												}
											}
	$ints2 = $in;
	$in=$in+1;
	$active_sheet->setCellValueByColumnAndRow(1, $in, 'Всего по подразделу:');
	$active_sheet->getStyle('A'.($in).':E'.($in))->applyFromArray($style_sum_all);
	$formulatv = '=SUM(E'.($int1).':E'.($ints2).')';
	$active_sheet->setCellValueByColumnAndRow(4, $in, $formulatv);
	$E[]='E'.($in);	
	$Erz[]='E'.($in);
		}
	}    
	if($Erz) {
	$Erzd = join("+", $Erz);
	unset($Erz);
	$ints3 = $in;
	$in=$in+1;
	$active_sheet->setCellValueByColumnAndRow(1, $in, 'Всего по разделу:');
	$active_sheet->getStyle('A'.($in).':E'.($in))->applyFromArray($style_sum_all);
		$Erzsum = '=('.$Erzd.')';
	$active_sheet->setCellValueByColumnAndRow(4, $in, $Erzsum);
	$Err[]='E'.($in);
	} else { continue; }
}	
		} 		
elseif ($categoryrow['template'] == '5' OR $categoryrow['template'] == '18') {
$docgl = $categoryrow['id'];
$rezgl = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='30'  AND value LIKE '%".$docgl."%'");
		
if($modx->db->getRecordCount($rezgl) >= 1 ) {
while ($rowgl = $modx->db->getRow($rezgl)) {
	$searchgl = stristr($rowgl['value'], '||');
		if(!empty($searchgl)) {
			$arrgl = explode("||", $rowgl['value']);
		$i2=0;
		foreach($arrgl as $val2) {
		$i2++;
		if($valgl==$docgl) {
				$idgl = $rowgl['contentid'];
				} 
			}
		} else { 
			if($rowgl['value']==$docgl) {
				$idgl = $rowgl['contentid'];
			} 
		}
	$idtvgl[] = $idgl;
	}
	$doptovgl = implode(",", $idtvgl);
$scidmscgl = substr($doptovgl, 0, -1);
$scidmscgl = trim($scidmscgl);
}
		if(!empty($idtvgl)){
$tovardop3 = $modx->db->select("id", "modx_site_content", 'parent = '.$docgl.' AND published = 1', "menuindex ASC");
		if($modx->db->getRecordCount($tovardop3) >= 1 ) {
    while($tovarr3 = $modx->db->getRow($tovardop3)) {
		$arrtov3[] =  $tovarr3['id'];
	}
			$tov_array3 = array_diff($arrtov3, array(''));
			$tov_line3 = implode(",", $tov_array3);
if(!empty($tov_line3)) { $zptr3 = ','; } else { $zptr3 = ''; }
			$doptovnw4 = $scidmscgl.''.$zptr3.''.$tov_line3;
			$doptovnw41 = trim($doptovnw4,",");
$doptov4 = ' id IN ('.$doptovnw41.') AND ';
$tovar3 = $modx->db->select("id,pagetitle,alias,menutitle,template,parent", "modx_site_content", $doptov4.'  published = 1 AND template = 6', "menuindex ASC");
		}
	} else {
$doptov4 = '';
$tovar3 = $modx->db->select("id,pagetitle,alias,menutitle,template,parent", "modx_site_content", 'parent = '.$docgl.' AND published = 1 AND template = 6', "menuindex ASC");
	}	
	unset($idtvv2);				
//$tovar3 = $modx->db->select("id,pagetitle,alias,menutitle,template,parent", "modx_site_content", 'parent = '.$docgl.' '.$doptov2.' AND published = 1', "menuindex ASC");
if($modx->db->getRecordCount($tovar3) >= 1 ) {
	$intv3=0;
    while($tovarrow3 = $modx->db->getRow($tovar3)) {
		$in++;
		$in1++;
		$in2++;
		$in3++;
		$intv3++;
		if($intv3==1) { $int3 = $in; }
		$tovlink =  'https://b-pro.com.ua'.$modx->makeUrl($tovarrow3['id']);
		$price  = $modx->runSnippet("DocInfo", array('docid' => $tovarrow3['id'], 'field' => 'price'));
		if(empty($price) or $price == 'цену уточняйте') { $price = 0; } else { $price = $price; }
		$nametv = $tovarrow3['pagetitle'];
		$col = 1;
		$active_sheet->setCellValueByColumnAndRow(0, $in, $intv3);
		$active_sheet->setCellValueByColumnAndRow(1, $in, $nametv);
		$active_sheet->getCell('B'.$in)->getHyperlink()->setUrl($tovlink);
		$active_sheet->setCellValueByColumnAndRow(2, $in, $col);
		$active_sheet->setCellValueByColumnAndRow(3, $in, $price);
		$formula = '=SUM(C'.($in).'*D'.($in).')';
		$active_sheet->setCellValueByColumnAndRow(4, $in, $formula);
		$active_sheet->getStyle('A'.($in))->applyFromArray($style_ncentr);
		$active_sheet->getStyle('B'.($in))->applyFromArray($style_ncentr);		
		$active_sheet->getStyle('C'.($in))->applyFromArray($style_centr);
		$active_sheet->getStyle('D'.($in))->applyFromArray($style_rcentr);
		$active_sheet->getStyle('E'.($in))->applyFromArray($style_rcentr);
										}
}
	$ints4 = $in;
	$in=$in+1;
	$active_sheet->setCellValueByColumnAndRow(1, $in, 'Всего по подразделу:');
	$active_sheet->getStyle('A'.($in).':E'.($in))->applyFromArray($style_sum_all);
		$formulatv = '=SUM(E'.($int3).':E'.($ints4).')';
	$active_sheet->setCellValueByColumnAndRow(4, $in, $formulatv);
	$E[]='E'.($in);	
		$Erzgl[]='E'.($in);	
		}
	if($Erz) {
		$Erzd = join("+", $Erz);
		unset($Erz);
	$ints3 = $in;
	$in=$in+1;
	$active_sheet->setCellValueByColumnAndRow(1, $in, 'Всего по разделу:');
	$active_sheet->getStyle('A'.($in).':E'.($in))->applyFromArray($style_sum_all);
		$Erzsum = '=('.$Erzd.')';
	$active_sheet->setCellValueByColumnAndRow(4, $in, $Erzsum);
	$Err[]='E'.($in);
	} else { continue; }
	}

}       
$nmfile = $name."_price_".date('d_m_Y').".xls";
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type:application/vnd.ms-excel');
        header('Content-Disposition:attachment;filename="'.$nmfile.'"');
        ob_end_clean(); 
        $objWriter->save('php://output');
        $result->free();
?>