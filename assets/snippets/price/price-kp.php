<?php
		$idglres = 5;
		//echo '..'.$idglres.'<br/>';

require_once('../../../manager/includes/config.inc.php');
require_once('../../../manager/includes/protect.inc.php');
define('MODX_API_MODE', true);
require_once('../../../manager/includes/document.parser.class.inc.php');
$modx = new DocumentParser;
$modx->db->connect();
$modx->getSettings();
include("../../../assets/modules/catalogFill/classes/PHPExcel.php");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
		//$active_sheet->setOddHeader('&CB-pro: прайс-лист');
        $active_sheet = $objPHPExcel->getActiveSheet();
	
        if(isset($_POST['kp'])) {  
        $active_sheet->getPageSetup()
            ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); // Ориентация страницы ORIENTATION_PORTRAIT
			} else {
		 $active_sheet->getPageSetup()
            ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT); // Ориентация страницы ORIENTATION_LANDSCAPE
			}
        $active_sheet->getPageSetup()
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4); // размер страницы формат А4

// массив стилей для заголовка таблицы
$style_wrap = array(
    // рамки
    'borders'=>array(
        // внешняя рамка
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'006464'
            )
        ),
        // внутренняя
        'allborders'=>array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'CCCCCC'
            )
        )
    ),
	    // шрифт
    'font'=>array(
        'bold' => true,
        'italic' => true,
        'size' => 12
    ),
	// выравнивание
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
    )
);

// массив стилей для таблицы
$style_wrapt = array(
    // рамки
    'borders'=>array(
        // внешняя рамка
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'006464'
            )
        ),
        // внутренняя
        'allborders'=>array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'CCCCCC'
            )
        )
    )

);
// массив стилей logo tel
$style_wrapv = array(
	'alignment' => array(
		'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
       	'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'rotation' => 0,
		'wrap' => true
    ),
        'font'=>array(
        'bold' => false,
        'size' => 12,
        'color'=>array(
            'rgb' => '000000'
        )
    )
);

$style_cat_6 = array( //fizika
    // Шрифт
    'font'=>array(
        'bold' => true,
        'size' => 13,
        'color'=>array(
            'rgb' => '000000'
        )
    ),
 	// рамки
    'borders'=>array(
        // внешняя рамка
	'bottom' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array(
		'	rgb' => '006464'
		)
	),
	'top' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array(
			'rgb' => '006464'
		)
	),
	'right' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array(
			'rgb' => '006464'
		)
	),
	'left' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array(
			'rgb' => '006464'
		)
	)
    )

);
$style_cat_7 = array( //himiya
    // Шрифт
    'font'=>array(
        'bold' => true,
        'size' => 15,
        'color'=>array(
            'rgb' => '000000'
        )
    ),
    // Заполнение цветом
    'fill' => array(
        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
        'color'=>array(
            'rgb' => '8DB4E3'
        )
    ),
			// рамки
    'borders'=>array(
        // внешняя рамка
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'006464'
            )
        )
    )

);
$style_pcat = array(
    // Шрифт
    'font'=>array(
        'bold' => true,
        'size' => 12,
        'color'=>array(
            'rgb' => '000000'
        )
    ),
    // Заполнение цветом
    'fill' => array(
        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
        'color'=>array(
            'rgb' => 'B8CCE4'
        )
    ),
			// рамки
    'borders'=>array(
        // внешняя рамка
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'006464'
            )
        )
    )

);
$style_all = array(
	// рамки
    'borders'=>array(
        // внешняя рамка
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'006464'
            )
			)
        ),
  
    // Шрифт
    'font'=>array(
        'bold' => true,
		'size' => 14,
        'color'=>array(
            'rgb' => '000000'
        )
    )

);
$style_dat = array(
    // Шрифт
    'font'=>array(
        'bold' => true,
        'size' => 12,
        'color'=>array(
            'rgb' => '000000'
        )
    )

);


        //Объединение ячеек (указываем диапазон объединения ячеек)
        //$active_sheet->mergeCells('A1:C1');    
        //Высота строки (также как и ширина столбцов указывается высота строки в пикселях)
        //$active_sheet->getRowDimension('1')->setRowHeight(30);
        //Вставить данные(примеры)
        //Нумерация строк начинается с 1, координаты A1 - 0,1
$style_centr = array(
    // выравнивание
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'wrap' => true,
		'rotation' => 0
    ),
	// рамки
    'borders'=>array(
        // внешняя рамка
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'006464'
            )
        )
    )
);
$style_ncentr = array(
    // выравнивание
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_LEFT,
		'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'wrap' => true,
		'rotation' => 0
    ),
		// рамки
    'borders'=>array(
        // внешняя рамка
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'006464'
            )
        )
    )
);
$style_rcentr = array(
    // выравнивание
    'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
		'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'wrap' => true,
		'rotation' => 0
    ),
		// рамки
    'borders'=>array(
        // внешняя рамка
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'006464'
            )
        )
    )
);
$style_ntop = array(
    // выравнивание
    'alignment' => array(
       	'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'rotation' => 0
    ),
		// рамки
    'borders'=>array(
        // внешняя рамка
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'006464'
            )
        )
    )
);

$style_rtop = array(
    // выравнивание
    'alignment' => array(
		'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
       	'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'rotation' => 0,
		'wrap' => true
    )
);
$style_rtext = array(
    // выравнивание
    'alignment' => array(
		'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_LEFT,
       	'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'rotation' => 0,
		'wrap' => true
    ),
	    // Шрифт
    'font'=>array(
        'bold' => false,
        'size' => 12,
        'color'=>array(
            'rgb' => '000000'
        )
	)
    
);
$style_cat_all = array( //fizika
    // Шрифт
    'font'=>array(
        'bold' => true,
        'size' => 15,
        'color'=>array(
            'rgb' => '000000'
        )
    ),
 			// рамки
    'borders'=>array(
        // внешняя рамка
        'outline' => array(
            'style'=>PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb'=>'006464'
            )
        )
    )

);
///logo
$objDrawing  = new PHPExcel_Worksheet_Drawing(); 
$objDrawing->setPath('../../../assets/images/price/bprologo.png'); 
$objDrawing->setCoordinates('A1'); 
$objDrawing->setOffsetX(15);
$objDrawing->setOffsetY(0);
$objDrawing->setRotation(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
// высота
$active_sheet->getRowDimension('1')->setRowHeight(65);
if(isset($_POST['kp']) and !empty($_POST['comerc'])) {  
$active_sheet->getRowDimension('5')->setRowHeight(60);
$active_sheet->getStyle('A5:I5')->applyFromArray($style_wrap);
 $active_sheet->setCellValueByColumnAndRow(0, 3, 'Дата '.date('d-m-Y'));
$active_sheet->getStyle('A3')->applyFromArray($style_dat);
		$active_sheet->mergeCells('A1:D1');
		$active_sheet->getStyle('E5')->getAlignment()->setWrapText(true);
		$active_sheet->getStyle('C5')->getAlignment()->setWrapText(true);
		$active_sheet->getStyle('D5')->getAlignment()->setWrapText(true);
		$active_sheet->getStyle('F5')->getAlignment()->setWrapText(true);
	$active_sheet->getStyle('F5')->getAlignment()->setWrapText(true);

		$active_sheet->getStyle('B1')->getAlignment()->setWrapText(true);
$active_sheet->getStyle('G1')->getAlignment()->setWrapText(true);
    $active_sheet->mergeCells('H1:I1');
        $active_sheet->setCellValue('H1', 'Тел.: +380 (044) 353-33-77'.chr(10).' Київстар: +380 (97) 748-05-98 '.chr(10).' Лайф: +380 (93) 324-54-94 '.chr(10).' МТС: +380 (95) 181-52-47');
//$active_sheet->getStyle('I1')->applyFromArray($style_dat);
		$active_sheet->getStyle('H1')->applyFromArray($style_wrapv);
//logo

// авто высота
//$active_sheet->getRowDimension(1)->setRowHeight(-1);
// авто ширина
//$active_sheet->getColumnDimension("A2")->setAutoSize(true);
        //Имя страницы Excel
        $active_sheet->setTitle("Комерційна пропозиція");
        //Ширина стобцов (указываем букву столбца и ширину в пикселях)
		$active_sheet->getColumnDimension('A')->setWidth(7);//№
		$active_sheet->getColumnDimension('B')->setWidth(9);//картинка
		$active_sheet->getColumnDimension('C')->setWidth(16);//картинка
        $active_sheet->getColumnDimension('D')->setWidth(38);//название
        $active_sheet->getColumnDimension('E')->setWidth(9);//К-ть
		 $active_sheet->getColumnDimension('F')->setWidth(15);//sobivart
        $active_sheet->getColumnDimension('G')->setWidth(15);//Ціна
        $active_sheet->getColumnDimension('H')->setWidth(15);//Сума
		$active_sheet->getColumnDimension('I')->setWidth(15);//Сума sobivart

        $active_sheet->setCellValue('A5', '№ п/п');
		$active_sheet->setCellValue('B5', 'Артикул');
		$active_sheet->setCellValue('C5', 'Зображення');
        $active_sheet->setCellValue('D5', 'Назва');
        $active_sheet->setCellValue('E5', 'К-ть'.chr(10).'шт');
		$active_sheet->setCellValue('F5', 'С/в'.chr(10).'(грн)');//sobivart
        $active_sheet->setCellValue('G5', 'Ціна'.chr(10).'(грн)');
		$active_sheet->setCellValue('H5', 'Сума с/в'.chr(10).'(грн)');//sobivart
		$active_sheet->setCellValue('I5', 'Сума '.chr(10).'(грн)');
 //print_r($_POST['comerc']);
$aDoor = $_POST['comerc'];
$N = count($aDoor);
$i = 0;
    for($i=0; $i < $N; $i++)
    {
	$pieces = explode("||", $aDoor[$i]);
	$pglcategory[]=$pieces[4];	
$category[]=$pieces[3];
$parentsy[]=$pieces[2];
$tovar[]=$pieces[0];
$idtt[]=$pieces[0]."||".$pieces[1];
$tovarpcat[]=$pieces[0]."||".$pieces[2];
	}
	$rescat = array_unique($category);
	$rescat = array_diff($rescat, array(''));
	
	foreach($rescat as $one){
		$idcat .= "id = '".$one."' or ";
	}
	$idcats = substr($idcat, 0, -3);
	$pglrescat = array_unique($pglcategory);
	$pglrescat = array_diff($pglrescat, array(''));
	
	foreach($pglrescat as $pglone){
		$pglidcat .= "id = '".$pglone."' or ";
	}
	$pglidcats = substr($pglidcat, 0, -3);	
/*	if(!empty($rescat)) {
$brcat = implode(",",$rescat);
	$scid1 = " id IN (".$brcat.") AND ";
	$scid2 = " AND contentid IN (".$brcat.")";
}
echo $brcat;*/

	$result = array_unique($parentsy);
	$result = array_diff($result, array(''));
	
	foreach($result as $one){
		$idpcat .= "id = '".$one."' or ";
	}
	$idpcats = substr($idpcat, 0, -3);
foreach($tovar as $tov){
		$ids[] = $tov;
	}
	$result = array_unique($ids);
	$result = array_diff($result, array(''));
if(!empty($result)) {
$br1 = implode(",",$result);
	$scid1 = " id IN (".$br1.") AND ";
	$scid2 = " AND contentid IN (".$br1.")";
}
	if(!empty($idpcats) and !empty($idcats)) {
		$or = " or ";
	}
$in = 0;
		$category = $modx->db->select("id,pagetitle,alias,menutitle,template", "modx_site_content", $pglidcats, "menuindex ASC");
if($modx->db->getRecordCount($category) >= 1 ) {
	
    while($categoryrow = $modx->db->getRow($category)) {
$in++;
if($in>1) { $in=$in+1; } else { $in=$in; }
$next = 5 + $in;
$nexta = $next+1;
$nextp = 5 + $in+1;
//$nextp = 5 + $in;
		$idcat = $categoryrow['id'];
		$style_cat= $style_cat_all;
		$active_sheet->setCellValueByColumnAndRow(0, $next, $categoryrow['pagetitle']);//Назва category
		$active_sheet->mergeCells('A'.($next).':I'.($next));
		$active_sheet->getStyle('A'.($next).':I'.($next))->applyFromArray($style_cat_7);
		if($categoryrow['template'] == 11) {
	$pcategory = $modx->db->select("id,parent,alias,pagetitle,menutitle,template", "modx_site_content", " parent='".$categoryrow['id']."' AND (".$idcats."".$or."".$idpcats.")" );
			if($modx->db->getRecordCount($pcategory) >= 1 ) {
				$nextt2 = 5 + $in+2;
    while($pcategoryrow = $modx->db->getRow($pcategory)) {
		$in++;
	$next = 5 + $in;	
$nextt = 5 + $in;
$nextt1 = $nextt+1;
//$nextt2 = $nextp+1;
		//echo "<p>".$pcategoryrow['pagetitle']."-".$idpcats."</p>";
			$style_cat= $style_cat_all;
		$active_sheet->setCellValueByColumnAndRow(0, $next, $pcategoryrow['pagetitle']);//Назва pcategory
		$active_sheet->mergeCells('A'.($next).':I'.($next));
		$active_sheet->getStyle('A'.($next).':I'.($next))->applyFromArray($style_cat_6);
		//////
		//if($pcategoryrow['template'] == 11) {
			$ppcategory = $modx->db->select("id,parent,alias,pagetitle,menutitle,template", "modx_site_content", " parent='".$pcategoryrow['id']."' AND (".$idpcats.")" );
		
			if($modx->db->getRecordCount($ppcategory) >= 1 ) {
				
    while($ppcategoryrow = $modx->db->getRow($ppcategory)) {
		$in++;
	$next = 5 + $in;	
$nextt = 5 + $in;
$nextt1 = $nextt+1;
//$nextt2 = $nextp+1;
		//echo "<p>".$pcategoryrow['pagetitle']."-".$idpcats."</p>";
			$style_cat= $style_cat_all;
		$active_sheet->setCellValueByColumnAndRow(0, $next, $ppcategoryrow['pagetitle']);//Назва pcategory
		$active_sheet->mergeCells('A'.($next).':I'.($next));
		$active_sheet->getStyle('A'.($next).':I'.($next))->applyFromArray($style_cat_6);
					$selecttv = $modx->db->query("SELECT contentid, value FROM modx_site_tmplvar_contentvalues WHERE tmplvarid = 30".$scid2);
//$rezultc1 = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='27'");
if($modx->db->getRecordCount($selecttv) >= 1 ) {
				foreach($selecttv as $rowc1) {
	$search = stristr($rowc1[value], '||');
	if(!empty($search)) {
		$arr = explode("||", $rowc1[value]);
			foreach($arr as $val) {
				if($val==$ppcategoryrow['id']) {
								foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1[contentid]) and ($piec3[1]==$val)) {
												$tovtvs = $rowc1[contentid];
												$tovtvsll[$ppcategoryrow['id']] .= $tovtvs.',';
									} 
								}//	
				} 
			} 
	} else {
		if($rowc1[value]==$ppcategoryrow['id']) {
			foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1[contentid]) and ($piec3[1]==$rowc1[value])) {
					$tovtvs = $rowc1[contentid];
					$tovtvsll[$ppcategoryrow['id']] .= $tovtvs.',';
									} 
		} // 
		}  
	} 
					unset($tovtvs);

}

}
$selectparent = $modx->db->select("id,parent", "modx_site_content", $scid1." parent='".$ppcategoryrow['id']."' AND published != '0'");
if($modx->db->getRecordCount($selectparent) > 0 ) {
    while($tovparent = $modx->db->getRow($selectparent)) {
		foreach($tovarpcat as $onepc1){//
			$piec31 = explode("||", $onepc1);
				if(($piec31[0]==$tovparent['id']) and ($piec31[1]==$tovparent['parent'])) {
						$tovtvs1 = $tovparent['id'].',';
				} else { $tovtvs1 ="";  
					   }
			$tovtvsll[$ppcategoryrow['id']] .= $tovtvs1;
		}//	
	}
}
	$tovtvsll[$ppcategoryrow['id']] = substr($tovtvsll[$ppcategoryrow['id']], 0, -1);
	if(!empty($tovtvsll[$ppcategoryrow['id']])) {
	$scid4 = "template = 6 AND id IN (".$tovtvsll[$ppcategoryrow['id']].") AND ";
	} //else { $scid4 = " id IN (".$scid1.") ";
	//$scid4 = " parent='".$categoryrow['id']."' "; 
	//}
$tovar = $modx->db->select("id,parent,alias,pagetitle", "modx_site_content", $scid4." published != '0'");
         if($modx->db->getRecordCount($tovar) >= 1 ) {
			 $nt=0;
    while($tovrow = $modx->db->getRow($tovar)) {//1 uroven
$in++;
$nt++;
		//if($intt>1) { $in=$in+1; $in=$in-1; } else { $in=$in; }
$nextt = 5 + $in;
$nextt1 = $nextt+1;////

	foreach($idtt as $ones){
$piecess = explode("||", $ones);
$idtv1=$piecess[0];
$colid=$piecess[1];
		if($idtv1==$tovrow['id']){
			$col=$colid;
		} 
	}
	$tovlink =  'https://b-pro.com.ua'.$modx->makeUrl($tovrow['id']);
		//$tovlink="https://b-pro.com.ua/katalog/".$categoryrow['alias']."/".$pcategoryrow['alias']."/".$tovrow['alias'];
//$col  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'col'));
		//if(empty($col)) { $col=1; }
		$idvl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'tovatatr'));//tovact		

$art  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'art'));
$image  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'image'));
        if(empty($image)) { $image = "assets/site/images/logo-ph.png"; } else { $image = $image; }
$vimir  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'vimir'));
$sebest  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'sebest'));
$price  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price'));
if(empty($price) or $price=='цену уточняйте') { $price='0'; } else { $price=$price;  }
$pricevhod  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price-vhod'));

$active_sheet->getRowDimension($nextt)->setRowHeight(50);
$objDrawingd  = new PHPExcel_Worksheet_Drawing(); 
$objDrawingd->setPath('../../../'.$image); 
$objDrawingd->setCoordinates('C'.$nextt); 
$objDrawingd->setWidth(70); 
$objDrawingd->setHeight(40);
$objDrawingd->setOffsetX(5);
$objDrawingd->setOffsetY(5);
$objDrawingd->setRotation(5);
$objDrawingd->setResizeProportional(true); 
$objDrawingd->setWidthAndHeight(70,40);
$objDrawingd->setWorksheet($objPHPExcel->getActiveSheet());

// высота
//$active_sheet->getStyle('H'.($nextt1))->getAlignment()->setWrapText(true);
$nametov1 = $tovrow['pagetitle'];
$nametov = strtr($nametov1, array(
    "'"=>'’'
 	));

$active_sheet->setCellValueByColumnAndRow(0, $nextt, $nt);//nn
		$active_sheet->setCellValueByColumnAndRow(1, $nextt, $art);//art
		$active_sheet->setCellValueByColumnAndRow(3, $nextt, $nametov);//1 uroven
		$active_sheet->getCell('D'.$nextt)->getHyperlink()->setUrl($tovlink);//link
		$active_sheet->setCellValueByColumnAndRow(4, $nextt, $col);//ART
	$active_sheet->setCellValueByColumnAndRow(5, $nextt, $sebest);//sobiv
	$active_sheet->setCellValueByColumnAndRow(6, $nextt, $price);//Од.вим.
	$formula = '=SUM(E'.($nextt).'*F'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(7, $nextt, $formula);//Собівартість formula
	$formula = '=SUM(E'.($nextt).'*G'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(8, $nextt, $formula);//Собівартість formula


$active_sheet->getStyle('A'.($nextt))->applyFromArray($style_ncentr);
$active_sheet->getStyle('B'.($nextt))->applyFromArray($style_centr);		
$active_sheet->getStyle('C'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('D'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('E'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('F'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('G'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('H'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('I'.($nextt))->applyFromArray($style_rcentr);

//$nextt1 = $nextt+1;
	}
			 	$active_sheet->setCellValueByColumnAndRow(1, $nextt1, 'Всього по розділу:');
	$active_sheet->getStyle('A'.($nextt1).':I'.($nextt1))->applyFromArray($style_all);
			//$active_sheet->setCellValueByColumnAndRow(5, $nextt1, $nextt1.'=='.$nextt2);
	$formulad1 = '=SUM(H'.($nextt2).':H'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(7, $nextt1, $formulad1);//Собівартість*/
$G[]='H'.($nextt1);	

	$formulad2 = '=SUM(I'.($nextt2).':I'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(8, $nextt1, $formulad2);//Собівартість*/
$F[]='I'.($nextt1);	
			}//pcat	
	}
			} else {
			$selecttv = $modx->db->query("SELECT contentid, value FROM modx_site_tmplvar_contentvalues WHERE tmplvarid = 30".$scid2);
//$rezultc1 = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='27'");
if($modx->db->getRecordCount($selecttv) >= 1 ) {
				foreach($selecttv as $rowc1) {
	$search = stristr($rowc1[value], '||');
	if(!empty($search)) {
		$arr = explode("||", $rowc1[value]);
			foreach($arr as $val) {
				if($val==$pcategoryrow['id']) {
								foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1[contentid]) and ($piec3[1]==$val)) {
												$tovtvs = $rowc1[contentid];
												$tovtvsll[$pcategoryrow['id']] .= $tovtvs.',';
									} 
								}//	
				} 
			} 
	} else {
		if($rowc1[value]==$pcategoryrow['id']) {
			foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1[contentid]) and ($piec3[1]==$rowc1[value])) {
					$tovtvs = $rowc1[contentid];
					$tovtvsll[$pcategoryrow['id']] .= $tovtvs.',';
									} 
		} // 
		}  
	} 
					unset($tovtvs);

}

}
$selectparent = $modx->db->select("id,parent", "modx_site_content", $scid1." parent='".$pcategoryrow['id']."' AND published != '0'");
if($modx->db->getRecordCount($selectparent) > 0 ) {
    while($tovparent = $modx->db->getRow($selectparent)) {
		foreach($tovarpcat as $onepc1){//
			$piec31 = explode("||", $onepc1);
				if(($piec31[0]==$tovparent['id']) and ($piec31[1]==$tovparent['parent'])) {
						$tovtvs1 = $tovparent['id'].',';
				} else { $tovtvs1 ="";  
					   }
			$tovtvsll[$pcategoryrow['id']] .= $tovtvs1;
		}//	
	}
}
	$tovtvsll[$pcategoryrow['id']] = substr($tovtvsll[$pcategoryrow['id']], 0, -1);
	if(!empty($tovtvsll[$pcategoryrow['id']])) {
	$scid4 = "template = 6 AND id IN (".$tovtvsll[$pcategoryrow['id']].") AND ";
	} //else { $scid4 = " id IN (".$scid1.") ";
	//$scid4 = " parent='".$categoryrow['id']."' "; 
	//}
$tovar = $modx->db->select("id,parent,alias,pagetitle", "modx_site_content", $scid4." published != '0'");
         if($modx->db->getRecordCount($tovar) >= 1 ) {
			 $nt=0;
    while($tovrow = $modx->db->getRow($tovar)) {//1 uroven
$in++;
$nt++;
		//if($intt>1) { $in=$in+1; $in=$in-1; } else { $in=$in; }
$nextt = 5 + $in;
$nextt1 = $nextt+1;////

	foreach($idtt as $ones){
$piecess = explode("||", $ones);
$idtv1=$piecess[0];
$colid=$piecess[1];
		if($idtv1==$tovrow['id']){
			$col=$colid;
		} 
	}
	$tovlink =  'https://b-pro.com.ua'.$modx->makeUrl($tovrow['id']);
		//$tovlink="https://b-pro.com.ua/katalog/".$categoryrow['alias']."/".$pcategoryrow['alias']."/".$tovrow['alias'];
//$col  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'col'));
		//if(empty($col)) { $col=1; }
		$idvl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'tovatatr'));//tovact		

$art  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'art'));
$image  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'image'));
        if(empty($image)) { $image = "assets/site/images/logo-ph.png"; } else { $image = $image; }
$vimir  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'vimir'));
$sebest  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'sebest'));
$price  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price'));
if(empty($price) or $price=='цену уточняйте') { $price='0'; } else { $price=$price;  }
$pricevhod  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price-vhod'));

$active_sheet->getRowDimension($nextt)->setRowHeight(50);
$objDrawingd  = new PHPExcel_Worksheet_Drawing(); 
$objDrawingd->setPath('../../../'.$image); 
$objDrawingd->setCoordinates('C'.$nextt); 
$objDrawingd->setWidth(70); 
$objDrawingd->setHeight(40);
$objDrawingd->setOffsetX(5);
$objDrawingd->setOffsetY(5);
$objDrawingd->setRotation(5);
$objDrawingd->setResizeProportional(true); 
$objDrawingd->setWidthAndHeight(70,40);
$objDrawingd->setWorksheet($objPHPExcel->getActiveSheet());

// высота
//$active_sheet->getStyle('H'.($nextt1))->getAlignment()->setWrapText(true);
$nametov1 = $tovrow['pagetitle'];
$nametov = strtr($nametov1, array(
    "'"=>'’'
 	));
$active_sheet->setCellValueByColumnAndRow(0, $nextt, $nt);//nn
		$active_sheet->setCellValueByColumnAndRow(1, $nextt, $art);//art
		$active_sheet->setCellValueByColumnAndRow(3, $nextt, $nametov);//1 uroven
		$active_sheet->getCell('D'.$nextt)->getHyperlink()->setUrl($tovlink);//link
		$active_sheet->setCellValueByColumnAndRow(4, $nextt, $col);//ART
	$active_sheet->setCellValueByColumnAndRow(5, $nextt, $sebest);//sobiv
	$active_sheet->setCellValueByColumnAndRow(6, $nextt, $price);//Од.вим.
	$formula = '=SUM(E'.($nextt).'*F'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(7, $nextt, $formula);//Собівартість formula
	$formula = '=SUM(E'.($nextt).'*G'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(8, $nextt, $formula);//Собівартість formula


$active_sheet->getStyle('A'.($nextt))->applyFromArray($style_ncentr);
$active_sheet->getStyle('B'.($nextt))->applyFromArray($style_centr);		
$active_sheet->getStyle('C'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('D'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('E'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('F'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('G'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('H'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('I'.($nextt))->applyFromArray($style_rcentr);

//$nextt1 = $nextt+1;
	}
	$active_sheet->setCellValueByColumnAndRow(1, $nextt1, 'Всього по розділу:');
	$active_sheet->getStyle('A'.($nextt1).':I'.($nextt1))->applyFromArray($style_all);
					//$active_sheet->setCellValueByColumnAndRow(5, $nextt1, $nextt1.'='.$nextt2);
	$formulad1 = '=SUM(H'.($nextt2).':H'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(7, $nextt1, $formulad1);//Собівартість*/
$G[]='H'.($nextt1);	

	$formulad2 = '=SUM(I'.($nextt2).':I'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(8, $nextt1, $formulad2);//Собівартість*/
$F[]='I'.($nextt1);		 
			}//pcat
			
		
		
			
		} ///
			
		//} 
	}
			
				
				
			} else {
				$pcategory = $modx->db->select("id,parent,alias,pagetitle,menutitle,template", "modx_site_content", " parent='".$categoryrow['id']."' AND (".$idpcats.")" );
			if($modx->db->getRecordCount($pcategory) >= 1 ) {
				$nextt2 = 5 + $in+2;
    while($pcategoryrow = $modx->db->getRow($pcategory)) {
	$in++;
//if($in>1) { $in=$in+1; } else { $in=$in; }
$next = 5 + $in;
$nexta = $next+1;
$nextp = 5 + $in+1;

//$nextp = 5 + $in;
		$idcat = $pcategoryrow['id'];
		$style_cat= $style_cat_all;
		$active_sheet->setCellValueByColumnAndRow(0, $next, $pcategoryrow['pagetitle']);//Назва category
		$active_sheet->mergeCells('A'.($next).':I'.($next));
		$active_sheet->getStyle('A'.($next).':I'.($next))->applyFromArray($style_cat_6);	
		$selecttv = $modx->db->query("SELECT contentid, value FROM modx_site_tmplvar_contentvalues WHERE tmplvarid = 30".$scid2);
//$rezultc1 = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='27'");
if($modx->db->getRecordCount($selecttv) >= 1 ) {
				foreach($selecttv as $rowc1) {
	$search = stristr($rowc1[value], '||');
	if(!empty($search)) {
		$arr = explode("||", $rowc1[value]);
			foreach($arr as $val) {
				if($val==$pcategoryrow['id']) {
								foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1[contentid]) and ($piec3[1]==$val)) {
												$tovtvs = $rowc1[contentid];
												$tovtvsll[$pcategoryrow['id']] .= $tovtvs.',';
									} 
								}//	
				} 
			} 
	} else {
		if($rowc1[value]==$pcategoryrow['id']) {
			foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1[contentid]) and ($piec3[1]==$rowc1[value])) {
					$tovtvs = $rowc1[contentid];
					$tovtvsll[$pcategoryrow['id']] .= $tovtvs.',';
									} 
		} // 
		}  
	} 
					unset($tovtvs);

}

}
$selectparent = $modx->db->select("id,parent", "modx_site_content", $scid1." parent='".$pcategoryrow['id']."' AND published != '0'");
if($modx->db->getRecordCount($selectparent) > 0 ) {
    while($tovparent = $modx->db->getRow($selectparent)) {
		foreach($tovarpcat as $onepc1){//
			$piec31 = explode("||", $onepc1);
				if(($piec31[0]==$tovparent['id']) and ($piec31[1]==$tovparent['parent'])) {
						$tovtvs1 = $tovparent['id'].',';
				} else { $tovtvs1 ="";  
					   }
			$tovtvsll[$pcategoryrow['id']] .= $tovtvs1;
		}//	
	}
}
	$tovtvsll[$pcategoryrow['id']] = substr($tovtvsll[$pcategoryrow['id']], 0, -1);
	if(!empty($tovtvsll[$pcategoryrow['id']])) {
	$scid4 = "template = 6 AND id IN (".$tovtvsll[$pcategoryrow['id']].") AND ";
	} //else { $scid4 = " id IN (".$scid1.") ";
	//$scid4 = " parent='".$categoryrow['id']."' "; 
	//}
$tovar = $modx->db->select("id,parent,alias,pagetitle", "modx_site_content", $scid4." published != '0'");
         if($modx->db->getRecordCount($tovar) >= 1 ) {
			 $nt=0;
    while($tovrow = $modx->db->getRow($tovar)) {//1 uroven
$in++;
$nt++;
		//if($intt>1) { $in=$in+1; $in=$in-1; } else { $in=$in; }
$nextt = 5 + $in;
$nextt1 = $nextt+1;////

	foreach($idtt as $ones){
$piecess = explode("||", $ones);
$idtv1=$piecess[0];
$colid=$piecess[1];
		if($idtv1==$tovrow['id']){
			$col=$colid;
		} 
	}
	$tovlink =  'https://b-pro.com.ua'.$modx->makeUrl($tovrow['id']);
		//$tovlink="https://b-pro.com.ua/katalog/".$categoryrow['alias']."/".$pcategoryrow['alias']."/".$tovrow['alias'];
//$col  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'col'));
		//if(empty($col)) { $col=1; }
		$idvl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'tovatatr'));//tovact		

$art  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'art'));
$image  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'image'));
         if(empty($image)) { $image = "assets/site/images/logo-ph.png"; } else { $image = $image; }
$vimir  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'vimir'));
$sebest  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'sebest'));
$price  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price'));
if(empty($price) or $price=='цену уточняйте') { $price='0'; } else { $price=$price;  }
$pricevhod  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price-vhod'));

$active_sheet->getRowDimension($nextt)->setRowHeight(50);
$objDrawingd  = new PHPExcel_Worksheet_Drawing(); 
$objDrawingd->setPath('../../../'.$image); 
$objDrawingd->setCoordinates('C'.$nextt); 
$objDrawingd->setWidth(70); 
$objDrawingd->setHeight(40);
$objDrawingd->setOffsetX(5);
$objDrawingd->setOffsetY(5);
$objDrawingd->setRotation(5);
$objDrawingd->setResizeProportional(true); 
$objDrawingd->setWidthAndHeight(70,40);
$objDrawingd->setWorksheet($objPHPExcel->getActiveSheet());
// высота
//$active_sheet->getStyle('H'.($nextt1))->getAlignment()->setWrapText(true);

$active_sheet->setCellValueByColumnAndRow(0, $nextt, $nt);//nn
		$active_sheet->setCellValueByColumnAndRow(1, $nextt, $art);//art
		$active_sheet->setCellValueByColumnAndRow(3, $nextt, $tovrow['pagetitle']);//1 uroven
		$active_sheet->getCell('D'.$nextt)->getHyperlink()->setUrl($tovlink);//link
		$active_sheet->setCellValueByColumnAndRow(4, $nextt, $col);//ART
	$active_sheet->setCellValueByColumnAndRow(5, $nextt, $sebest);//sobiv
	$active_sheet->setCellValueByColumnAndRow(6, $nextt, $price);//Од.вим.
	$formula = '=SUM(E'.($nextt).'*F'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(7, $nextt, $formula);//Собівартість formula
	$formula = '=SUM(E'.($nextt).'*G'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(8, $nextt, $formula);//Собівартість formula


$active_sheet->getStyle('A'.($nextt))->applyFromArray($style_ncentr);
$active_sheet->getStyle('B'.($nextt))->applyFromArray($style_centr);		
$active_sheet->getStyle('C'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('D'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('E'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('F'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('G'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('H'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('I'.($nextt))->applyFromArray($style_rcentr);

//$nextt1 = $nextt+1;
	}
			}//pcat
		//++++
		
	}
			}
				$active_sheet->setCellValueByColumnAndRow(1, $nextt1, 'Всього по розділу:');
	$active_sheet->getStyle('A'.($nextt1).':I'.($nextt1))->applyFromArray($style_all);
					//$active_sheet->setCellValueByColumnAndRow(5, $nextt1, $nextt1.'='.$nextt2);
	$formulad1 = '=SUM(H'.($nextt).':H'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(7, $nextt1, $formulad1);//Собівартість*/
$G[]='H'.($nextt1);	

	$formulad2 = '=SUM(I'.($nextt2).':I'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(8, $nextt1, $formulad2);//Собівартість*/
$F[]='I'.($nextt1);
			}
		} else { 
		//if($categoryrow['template'] == 5) {
$selecttv = $modx->db->query("SELECT contentid, value FROM modx_site_tmplvar_contentvalues WHERE tmplvarid = 30".$scid2);
//$rezultc1 = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='27'");
if($modx->db->getRecordCount($selecttv) >= 1 ) {
				foreach($selecttv as $rowc1) {
	$search = stristr($rowc1[value], '||');
	if(!empty($search)) {
		$arr = explode("||", $rowc1[value]);
			foreach($arr as $val) {
				if($val==$categoryrow['id']) {
								foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1[contentid]) and ($piec3[1]==$val)) {
												$tovtvs = $rowc1[contentid];
												$tovtvsll[$categoryrow['id']] .= $tovtvs.',';
									} 
								}//	
				} 
			} 
	} else {
		if($rowc1[value]==$categoryrow['id']) {
			foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1[contentid]) and ($piec3[1]==$rowc1[value])) {
					$tovtvs = $rowc1[contentid];
					$tovtvsll[$categoryrow['id']] .= $tovtvs.',';
									} 
		} // 
		}  
	} 
					unset($tovtvs);

}

}
$selectparent = $modx->db->select("id,parent", "modx_site_content", $scid1." parent='".$categoryrow['id']."' AND published != '0'");
if($modx->db->getRecordCount($selectparent) > 0 ) {
    while($tovparent = $modx->db->getRow($selectparent)) {
		foreach($tovarpcat as $onepc1){//
			$piec31 = explode("||", $onepc1);
				if(($piec31[0]==$tovparent['id']) and ($piec31[1]==$tovparent['parent'])) {
						$tovtvs1 = $tovparent['id'].',';
				} else { $tovtvs1 ="";  
					   }
			$tovtvsll[$categoryrow['id']] .= $tovtvs1;
		}//	
	}
}
	$tovtvsll[$categoryrow['id']] = substr($tovtvsll[$categoryrow['id']], 0, -1);
	if(!empty($tovtvsll[$categoryrow['id']])) {
	$scid4 = "template = 6 AND id IN (".$tovtvsll[$categoryrow['id']].") AND ";
	} //else { $scid4 = " id IN (".$scid1.") ";
	//$scid4 = " parent='".$categoryrow['id']."' "; 
	//}
$tovar = $modx->db->select("id,parent,alias,pagetitle", "modx_site_content", $scid4." published != '0'");
         if($modx->db->getRecordCount($tovar) >= 1 ) {
			 $nt=0;
    while($tovrow = $modx->db->getRow($tovar)) {//1 uroven
$in++;
$nt++;
		//if($intt>1) { $in=$in+1; $in=$in-1; } else { $in=$in; }
$nextt = 5 + $in;
$nextt1 = $nextt+1;////
$nextt2 = $nextp-1;
	foreach($idtt as $ones){
$piecess = explode("||", $ones);
$idtv1=$piecess[0];
$colid=$piecess[1];
		if($idtv1==$tovrow['id']){
			$col=$colid;
		} 
	}
	$tovlink =  'https://b-pro.com.ua'.$modx->makeUrl($tovrow['id']);
		//$tovlink="https://b-pro.com.ua/katalog/".$categoryrow['alias']."/".$pcategoryrow['alias']."/".$tovrow['alias'];
//$col  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'col'));
		//if(empty($col)) { $col=1; }
		$idvl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'tovatatr'));//tovact		

$art  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'art'));
$image  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'image'));
        if(empty($image)) { $image = "assets/site/images/logo-ph.png"; } else { $image = $image; }
$vimir  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'vimir'));
$sebest  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'sebest'));
$price  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price'));
if(empty($price) or $price=='цену уточняйте') { $price='0'; } else { $price=$price;  }
$pricevhod  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price-vhod'));

$active_sheet->getRowDimension($nextt)->setRowHeight(50);
$objDrawingd  = new PHPExcel_Worksheet_Drawing(); 
$objDrawingd->setPath('../../../'.$image); 
$objDrawingd->setCoordinates('C'.$nextt); 
$objDrawingd->setWidth(70); 
$objDrawingd->setHeight(40);
$objDrawingd->setOffsetX(5);
$objDrawingd->setOffsetY(5);
$objDrawingd->setRotation(5);
$objDrawingd->setResizeProportional(true); 
$objDrawingd->setWidthAndHeight(70,40);
$objDrawingd->setWorksheet($objPHPExcel->getActiveSheet());
// высота
//$active_sheet->getStyle('H'.($nextt1))->getAlignment()->setWrapText(true);

$active_sheet->setCellValueByColumnAndRow(0, $nextt, $nt);//nn
		$active_sheet->setCellValueByColumnAndRow(1, $nextt, $art);//art
		$active_sheet->setCellValueByColumnAndRow(3, $nextt, $tovrow['pagetitle']);//1 uroven
		$active_sheet->getCell('D'.$nextt)->getHyperlink()->setUrl($tovlink);//link
		$active_sheet->setCellValueByColumnAndRow(4, $nextt, $col);//ART
	$active_sheet->setCellValueByColumnAndRow(5, $nextt, $sebest);//sobiv
	$active_sheet->setCellValueByColumnAndRow(6, $nextt, $price);//Од.вим.
	$formula = '=SUM(E'.($nextt).'*F'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(7, $nextt, $formula);//Собівартість formula
	$formula = '=SUM(E'.($nextt).'*G'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(8, $nextt, $formula);//Собівартість formula


$active_sheet->getStyle('A'.($nextt))->applyFromArray($style_ncentr);
$active_sheet->getStyle('B'.($nextt))->applyFromArray($style_centr);		
$active_sheet->getStyle('C'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('D'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('E'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('F'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('G'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('H'.($nextt))->applyFromArray($style_rcentr);
$active_sheet->getStyle('I'.($nextt))->applyFromArray($style_rcentr);

//$nextt1 = $nextt+1;
	}
			 //$nextt2 = 5 + $in+2;
	$active_sheet->setCellValueByColumnAndRow(1, $nextt1, 'Всього по розділу:');
	$active_sheet->getStyle('A'.($nextt1).':I'.($nextt1))->applyFromArray($style_all);
			//$active_sheet->setCellValueByColumnAndRow(5, $nextt1, $nextt1.'=='.$nextt2);
	$formulad1 = '=SUM(H'.($nextt2).':H'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(7, $nextt1, $formulad1);//Собівартість*/
$G[]='H'.($nextt1);	

	$formulad2 = '=SUM(I'.($nextt2).':I'.($nextt).')';
	$active_sheet->setCellValueByColumnAndRow(8, $nextt1, $formulad2);//Собівартість*/
$F[]='I'.($nextt1);	
			}//pcat
		}//5 в главном разделе	
	}
	
}
//Всього
	if($G) {
		$Gs = join("+", $G);
			}
		else {
			return;
		}
		if($F) {
		$Fs = join("+", $F);
			}
		else {
			return;
		}
	$nextt12=$nextt1+1;
	$active_sheet->setCellValueByColumnAndRow(1, $nextt12, 'Загальна вартість:');
	$active_sheet->getStyle('A'.($nextt12).':I'.($nextt12))->applyFromArray($style_all);
	$Fsum = '=('.$Fs.')';
	$Gsum = '=('.$Gs.')';
	$active_sheet->setCellValueByColumnAndRow(7, $nextt12, $Gsum);//*/$nextt1
	$active_sheet->setCellValueByColumnAndRow(8, $nextt12, $Fsum);//*/$nextt1
$nmfile='kp.xls';

	}//tehn
 elseif(isset($_POST['tz']) and !empty($_POST['teh'])) { 
		//$active_sheet->mergeCells('A1:D2');
		$active_sheet->mergeCells('A3:E3');
		$active_sheet->getStyle('B1')->getAlignment()->setWrapText(true);
		$active_sheet->getStyle('A7:E7')->applyFromArray($style_wrap);
$style_tehz = array(
	'alignment' => array(
		'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
       	'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'rotation' => 0,
		'wrap' => true
    ),
//Шрифт
	'font'=>array(
		'bold' => true,
		'size' => 18
	)
);
		$active_sheet->mergeCells('A4:E4');
        $active_sheet->setCellValue('E1', 'Додаток 4'.chr(10).'тендерної документації');
		$active_sheet->getStyle('E1')->getAlignment()->setWrapText(true);
		$active_sheet->getStyle('E1')->applyFromArray($style_rtop);
		$active_sheet->setCellValue('A3', 'ТЕХНІЧНЕ ЗАВДАННЯ');
		
		$active_sheet->setCellValue('A4', 'код ДК 021:2015 – 30210000-4 Машини для обробки даних (апаратна частина) (системні блоки)');
$style_teh = array(
	'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
		'rotation' => 0
    ),				   
	//Шрифт
	'font'=>array(
		'bold' => true,
		'size' => 20
	),
//Выравнивание
	'alignment' => array(
		'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
		'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
	)
);
$active_sheet->getStyle('A3')->applyFromArray($style_teh);
//$active_sheet->getRowDimension(4)->setRowHeight(-1);
$active_sheet->getStyle('A4')->getAlignment()->setWrapText(true);
$active_sheet->getStyle('C7')->getAlignment()->setWrapText(true);
$active_sheet->getStyle('A4')->applyFromArray($style_tehz);
$active_sheet->getRowDimension('4')->setRowHeight(55);


        //Имя страницы Excel
        $active_sheet->setTitle("ТЕХНІЧНЕ ЗАВДАННЯ");
        //Ширина стобцов (указываем букву столбца и ширину в пикселях)
        $active_sheet->getColumnDimension('A')->setWidth(8);
        $active_sheet->getColumnDimension('B')->setWidth(24);
        $active_sheet->getColumnDimension('C')->setWidth(6);
        $active_sheet->getColumnDimension('D')->setWidth(7);
        $active_sheet->getColumnDimension('E')->setWidth(41);
 

        $active_sheet->setCellValue('A7', '№');
		$active_sheet->setCellValue('B7', 'Найменування');
		$active_sheet->setCellValue('C7', 'Од. '.chr(10).'вим.');
        $active_sheet->setCellValue('D7', 'К-ть');
        $active_sheet->setCellValue('E7', 'Опис');
  
$aDoort = $_POST['teh'];
$N = count($aDoort);
$i = 0;
    for($i=0; $i < $N; $i++)
    {
	$pieces = explode("||", $aDoort[$i]);
	$pglcategory[]=$pieces[4];	
$category[]=$pieces[3];
$parentsy[]=$pieces[2];
$tovar[]=$pieces[0];
$idtt[]=$pieces[0]."||".$pieces[1];
$tovarpcat[]=$pieces[0]."||".$pieces[2];
	}
	$rescat = array_unique($category);
	$rescat = array_diff($rescat, array(''));
	
	foreach($rescat as $one){
		$idcat .= "id = '".$one."' or ";
	}
	$idcats = substr($idcat, 0, -3);
	$pglrescat = array_unique($pglcategory);
	$pglrescat = array_diff($pglrescat, array(''));
	
	foreach($pglrescat as $pglone){
		$pglidcat .= "id = '".$pglone."' or ";
	}
	$pglidcats = substr($pglidcat, 0, -3);	
/*	if(!empty($rescat)) {
$brcat = implode(",",$rescat);
	$scid1 = " id IN (".$brcat.") AND ";
	$scid2 = " AND contentid IN (".$brcat.")";
}
echo $brcat;*/

	$result = array_unique($parentsy);
	$result = array_diff($result, array(''));
	
	foreach($result as $one){
		$idpcat .= "id = '".$one."' or ";
	}
	$idpcats = substr($idpcat, 0, -3);
foreach($tovar as $tov){
		$ids[] = $tov;
	}
	$result = array_unique($ids);
	$result = array_diff($result, array(''));
if(!empty($result)) {
$br1 = implode(",",$result);
	$scid1 = " id IN (".$br1.") AND ";
	$scid2 = " AND contentid IN (".$br1.")";
}
	if(!empty($idpcats) and !empty($idcats)) {
		$or = " or ";
	}
$in = 0;
		$category = $modx->db->select("id,pagetitle,alias,menutitle,template", "modx_site_content", $pglidcats, "menuindex ASC");
if($modx->db->getRecordCount($category) >= 1 ) {
	
    while($categoryrow = $modx->db->getRow($category)) {
$in++;
if($in>1) { $in=$in+1; } else { $in=$in; }
$next = 7 + $in;
$nexta = $next+1;
$nextp = 7 + $in+1;
//$nextp = 7 + $in;
		$idcat = $categoryrow['id'];
		$style_cat= $style_cat_all;
		$active_sheet->setCellValueByColumnAndRow(0, $next, $categoryrow['pagetitle']);//Назва category
		$active_sheet->mergeCells('A'.($next).':E'.($next));
		$active_sheet->getStyle('A'.($next).':E'.($next))->applyFromArray($style_cat_7);
		if($categoryrow['template'] == 11) {
	$pcategory = $modx->db->select("id,parent,alias,pagetitle,menutitle,template", "modx_site_content", " parent='".$categoryrow['id']."' AND (".$idcats."".$or."".$idpcats.")" );
			if($modx->db->getRecordCount($pcategory) >= 1 ) {
				$nextt2 = 7 + $in+2;
    while($pcategoryrow = $modx->db->getRow($pcategory)) {
		$in++;
	$next = 7 + $in;	
$nextt = 7 + $in;
$nextt1 = $nextt+1;
//$nextt2 = $nextp+1;
		//echo "<p>".$pcategoryrow['pagetitle']."-".$idpcats."</p>";
			$style_cat= $style_cat_all;
		$active_sheet->setCellValueByColumnAndRow(0, $next, $pcategoryrow['pagetitle']);//Назва pcategory
		$active_sheet->mergeCells('A'.($next).':E'.($next));
		$active_sheet->getStyle('A'.($next).':E'.($next))->applyFromArray($style_cat_6);
		//////
		//if($pcategoryrow['template'] == 11) {
			$ppcategory = $modx->db->select("id,parent,alias,pagetitle,menutitle,template", "modx_site_content", " parent='".$pcategoryrow['id']."' AND (".$idpcats.")" );
		
			if($modx->db->getRecordCount($ppcategory) >= 1 ) {
				
    while($ppcategoryrow = $modx->db->getRow($ppcategory)) {
		$in++;
	$next = 7 + $in;	
$nextt = 7 + $in;
$nextt1 = $nextt+1;
//$nextt2 = $nextp+1;
		//echo "<p>".$pcategoryrow['pagetitle']."-".$idpcats."</p>";
			$style_cat= $style_cat_all;
		$active_sheet->setCellValueByColumnAndRow(0, $next, $ppcategoryrow['pagetitle']);//Назва pcategory
		$active_sheet->mergeCells('A'.($next).':E'.($next));
		$active_sheet->getStyle('A'.($next).':E'.($next))->applyFromArray($style_cat_6);
					$selecttv = $modx->db->query("SELECT contentid, value FROM modx_site_tmplvar_contentvalues WHERE tmplvarid = 30".$scid2);
//$rezultc1 = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='27'");
if($modx->db->getRecordCount($selecttv) >= 1 ) {
				foreach($selecttv as $rowc1) {
	$search = stristr($rowc1['value'], '||');

	if(!empty($search)) {
		$arr = explode("||", $rowc1['value']);
			foreach($arr as $val) {
				if($val==$ppcategoryrow['id']) {
								foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1['contentid']) and ($piec3[1]==$val)) {
												$tovtvs = $rowc1['contentid'];
												$tovtvsll[$ppcategoryrow['id']] .= $tovtvs.',';
									} 
								}//	
				} 
			} 
	} else {
		if($rowc1['value']==$ppcategoryrow['id']) {
			foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1['contentid']) and ($piec3[1]==$rowc1['value'])) {
					$tovtvs = $rowc1['contentid'];
					$tovtvsll[$ppcategoryrow['id']] .= $tovtvs.',';
									} 
		} // 
		}  
	} 
					unset($tovtvs);

}

}
$selectparent = $modx->db->select("id,parent", "modx_site_content", $scid1." parent='".$ppcategoryrow['id']."' AND published != '0'");
if($modx->db->getRecordCount($selectparent) > 0 ) {
    while($tovparent = $modx->db->getRow($selectparent)) {
		foreach($tovarpcat as $onepc1){//
			$piec31 = explode("||", $onepc1);
				if(($piec31[0]==$tovparent['id']) and ($piec31[1]==$tovparent['parent'])) {
						$tovtvs1 = $tovparent['id'].',';
				} else { $tovtvs1 ="";  
					   }
			$tovtvsll[$ppcategoryrow['id']] .= $tovtvs1;
		}//	
	}
}
	$tovtvsll[$ppcategoryrow['id']] = substr($tovtvsll[$ppcategoryrow['id']], 0, -1);
	if(!empty($tovtvsll[$ppcategoryrow['id']])) {
	$scid4 = " id IN (".$tovtvsll[$ppcategoryrow['id']].") AND ";
	} //else { $scid4 = " id IN (".$scid1.") ";
	//$scid4 = " parent='".$categoryrow['id']."' "; 
	//}
$tovar = $modx->db->select("id,parent,alias,pagetitle", "modx_site_content", $scid4." published != '0'");
         if($modx->db->getRecordCount($tovar) >= 1 ) {
			 $nt=0;
    while($tovrow = $modx->db->getRow($tovar)) {//1 uroven
$in++;
$nt++;
		//if($intt>1) { $in=$in+1; $in=$in-1; } else { $in=$in; }
$nextt = 7 + $in;
$nextt1 = $nextt+1;////

	foreach($idtt as $ones){
$piecess = explode("||", $ones);
$idtv1=$piecess[0];
$colid=$piecess[1];
		if($idtv1==$tovrow['id']){
			$col=$colid;
		} 
	}
	$tovlink =  'https://b-pro.com.ua'.$modx->makeUrl($tovrow['id']);
		//$tovlink="https://b-pro.com.ua/katalog/".$categoryrow['alias']."/".$pcategoryrow['alias']."/".$tovrow['alias'];
//$col  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'col'));
		//if(empty($col)) { $col=1; }
$idvl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'tovatatr'));//tovact		
		//$tovlink="https://b-pro.com.ua/katalog/".$categoryrow['alias']."/".$pcategoryrow11['alias']."/".$pcategoryrow11['alias']."/".$tovrow['alias'];
		$tovlink =  'https://b-pro.com.ua'.$modx->makeUrl($tovrow['id']);
$vimir  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'vimir'));
if(empty($vimir)) { $vimir='шт'; }
$price  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price'));
		if(empty($price) or $price=='цену уточняйте') { $price='0'; } else { $price=$price;  }
$compl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'opistz'));
	$active_sheet->getStyle('B'.($nextt))->getAlignment()->setWrapText(true);
$active_sheet->getStyle('A'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('B'.($nextt))->applyFromArray($style_ncentr);
$active_sheet->getStyle('C'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('D'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('E'.($nextt))->applyFromArray($style_ncentr);

	$active_sheet->setCellValueByColumnAndRow(0, $nextt, $nt);//nn
	$active_sheet->setCellValueByColumnAndRow(1, $nextt, $tovrow['pagetitle']);//Назва 
	$active_sheet->getCell('B'.$nextt)->getHyperlink()->setUrl($tovlink);//link
	$active_sheet->setCellValueByColumnAndRow(2, $nextt, $vimir);//Од.вим.
	$active_sheet->setCellValueByColumnAndRow(3, $nextt, $col);//Кіль
	$active_sheet->setCellValueByColumnAndRow(4, $nextt, strip_tags(html_entity_decode($compl)));//compl
	
	$active_sheet->getStyle('E'.($nextt))->getAlignment()->setWrapText(true);

//$nextt1 = $nextt+1;
	}

			}//pcat	
	}
			} else {
			$selecttv = $modx->db->query("SELECT contentid, value FROM modx_site_tmplvar_contentvalues WHERE tmplvarid = 30".$scid2);
//$rezultc1 = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='27'");
if($modx->db->getRecordCount($selecttv) >= 1 ) {
				foreach($selecttv as $rowc1) {
	$search = stristr($rowc1['value'], '||');
	if(!empty($search)) {
		$arr = explode("||", $rowc1['value']);
			foreach($arr as $val) {
				if($val==$pcategoryrow['id']) {
								foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1['contentid']) and ($piec3[1]==$val)) {
												$tovtvs = $rowc1['contentid'];
												$tovtvsll[$pcategoryrow['id']] .= $tovtvs.',';
									} 
								}//	
				} 
			} 
	} else {
		if($rowc1['value']==$pcategoryrow['id']) {
			foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1['contentid']) and ($piec3[1]==$rowc1['value'])) {
					$tovtvs = $rowc1['contentid'];
					$tovtvsll[$pcategoryrow['id']] .= $tovtvs.',';
									} 
		} // 
		}  
	} 
					unset($tovtvs);

}

}
$selectparent = $modx->db->select("id,parent", "modx_site_content", $scid1." parent='".$pcategoryrow['id']."' AND published != '0'");
if($modx->db->getRecordCount($selectparent) > 0 ) {
    while($tovparent = $modx->db->getRow($selectparent)) {
		foreach($tovarpcat as $onepc1){//
			$piec31 = explode("||", $onepc1);
				if(($piec31[0]==$tovparent['id']) and ($piec31[1]==$tovparent['parent'])) {
						$tovtvs1 = $tovparent['id'].',';
				} else { $tovtvs1 ="";  
					   }
			$tovtvsll[$pcategoryrow['id']] .= $tovtvs1;
		}//	
	}
}
	$tovtvsll[$pcategoryrow['id']] = substr($tovtvsll[$pcategoryrow['id']], 0, -1);
	if(!empty($tovtvsll[$pcategoryrow['id']])) {
	$scid4 = " id IN (".$tovtvsll[$pcategoryrow['id']].") AND ";
	} //else { $scid4 = " id IN (".$scid1.") ";
	//$scid4 = " parent='".$categoryrow['id']."' "; 
	//}
$tovar = $modx->db->select("id,parent,alias,pagetitle", "modx_site_content", $scid4." published != '0'");
         if($modx->db->getRecordCount($tovar) >= 1 ) {
			 $nt=0;
    while($tovrow = $modx->db->getRow($tovar)) {//1 uroven
$in++;
$nt++;
		//if($intt>1) { $in=$in+1; $in=$in-1; } else { $in=$in; }
$nextt = 7 + $in;
$nextt1 = $nextt+1;////

	foreach($idtt as $ones){
$piecess = explode("||", $ones);
$idtv1=$piecess[0];
$colid=$piecess[1];
		if($idtv1==$tovrow['id']){
			$col=$colid;
		} 

	}
$idvl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'tovatatr'));//tovact		
		$tovlink="https://b-pro.com.ua/katalog/".$categoryrow['alias']."/".$pcategoryrow11['alias']."/".$pcategoryrow11['alias']."/".$tovrow['alias'];
$vimir  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'vimir'));
if(empty($vimir)) { $vimir='шт'; }
$price  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price'));
		if(empty($price) or $price=='цену уточняйте') { $price='0'; } else { $price=$price;  }
$compl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'opistz'));
	$active_sheet->getStyle('B'.($nextt))->getAlignment()->setWrapText(true);
$active_sheet->getStyle('A'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('B'.($nextt))->applyFromArray($style_ncentr);
$active_sheet->getStyle('C'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('D'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('E'.($nextt))->applyFromArray($style_ncentr);

	$active_sheet->setCellValueByColumnAndRow(0, $nextt, $nt);//nn
	$active_sheet->setCellValueByColumnAndRow(1, $nextt, $tovrow['pagetitle']);//Назва 
	$active_sheet->getCell('B'.$nextt)->getHyperlink()->setUrl($tovlink);//link
	$active_sheet->setCellValueByColumnAndRow(2, $nextt, $vimir);//Од.вим.
	$active_sheet->setCellValueByColumnAndRow(3, $nextt, $col);//Кіль
	$active_sheet->setCellValueByColumnAndRow(4, $nextt, strip_tags(html_entity_decode($compl)));//compl
	
	$active_sheet->getStyle('E'.($nextt))->getAlignment()->setWrapText(true);

//$nextt1 = $nextt+1;
	}

			}//pcat
			
		
		
			
		} ///
			
		//} 
	}
			
				
				
			} else {
				$pcategory = $modx->db->select("id,parent,alias,pagetitle,menutitle,template", "modx_site_content", " parent='".$categoryrow['id']."' AND (".$idpcats.")" );
			if($modx->db->getRecordCount($pcategory) >= 1 ) {
				$nextt2 = 7 + $in+2;
    while($pcategoryrow = $modx->db->getRow($pcategory)) {
	$in++;
//if($in>1) { $in=$in+1; } else { $in=$in; }
$next = 7 + $in;
$nexta = $next+1;
$nextp = 7 + $in+1;

//$nextp = 7 + $in;
		$idcat = $pcategoryrow['id'];
		$style_cat= $style_cat_all;
		$active_sheet->setCellValueByColumnAndRow(0, $next, $pcategoryrow['pagetitle']);//Назва category
		$active_sheet->mergeCells('A'.($next).':I'.($next));
		$active_sheet->getStyle('A'.($next).':I'.($next))->applyFromArray($style_cat_6);	
		$selecttv = $modx->db->query("SELECT contentid, value FROM modx_site_tmplvar_contentvalues WHERE tmplvarid = 30".$scid2);
//$rezultc1 = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='27'");
if($modx->db->getRecordCount($selecttv) >= 1 ) {
				foreach($selecttv as $rowc1) {
	$search = stristr($rowc1['value'], '||');
	if(!empty($search)) {
		$arr = explode("||", $rowc1['value']);
			foreach($arr as $val) {
				if($val==$pcategoryrow['id']) {
								foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1['contentid']) and ($piec3[1]==$val)) {
												$tovtvs = $rowc1['contentid'];
												$tovtvsll[$pcategoryrow['id']] .= $tovtvs.',';
									} 
								}//	
				} 
			} 
	} else {
		if($rowc1['value']==$pcategoryrow['id']) {
			foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1['contentid']) and ($piec3[1]==$rowc1['value'])) {
					$tovtvs = $rowc1['contentid'];
					$tovtvsll[$pcategoryrow['id']] .= $tovtvs.',';
									} 
		} // 
		}  
	} 
					unset($tovtvs);

}

}
$selectparent = $modx->db->select("id,parent", "modx_site_content", $scid1." parent='".$pcategoryrow['id']."' AND published != '0'");
if($modx->db->getRecordCount($selectparent) > 0 ) {
    while($tovparent = $modx->db->getRow($selectparent)) {
		foreach($tovarpcat as $onepc1){//
			$piec31 = explode("||", $onepc1);
				if(($piec31[0]==$tovparent['id']) and ($piec31[1]==$tovparent['parent'])) {
						$tovtvs1 = $tovparent['id'].',';
				} else { $tovtvs1 ="";  
					   }
			$tovtvsll[$pcategoryrow['id']] .= $tovtvs1;
		}//	
	}
}
	$tovtvsll[$pcategoryrow['id']] = substr($tovtvsll[$pcategoryrow['id']], 0, -1);
	if(!empty($tovtvsll[$pcategoryrow['id']])) {
	$scid4 = " id IN (".$tovtvsll[$pcategoryrow['id']].") AND ";
	} //else { $scid4 = " id IN (".$scid1.") ";
	//$scid4 = " parent='".$categoryrow['id']."' "; 
	//}
$tovar = $modx->db->select("id,parent,alias,pagetitle", "modx_site_content", $scid4." published != '0'");
         if($modx->db->getRecordCount($tovar) >= 1 ) {
			 $nt=0;
    while($tovrow = $modx->db->getRow($tovar)) {//1 uroven
$in++;
$nt++;
		//if($intt>1) { $in=$in+1; $in=$in-1; } else { $in=$in; }
$nextt = 7 + $in;
$nextt1 = $nextt+1;////

	foreach($idtt as $ones){
$piecess = explode("||", $ones);
$idtv1=$piecess[0];
$colid=$piecess[1];
		if($idtv1==$tovrow['id']){
			$col=$colid;
		} 
	}
$idvl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'tovatatr'));//tovact		
		$tovlink="https://b-pro.com.ua/katalog/".$categoryrow['alias']."/".$pcategoryrow11['alias']."/".$pcategoryrow11['alias']."/".$tovrow['alias'];
$vimir  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'vimir'));
if(empty($vimir)) { $vimir='шт'; }
$price  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price'));
		if(empty($price) or $price=='цену уточняйте') { $price='0'; } else { $price=$price;  }
$compl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'opistz'));
	$active_sheet->getStyle('B'.($nextt))->getAlignment()->setWrapText(true);
$active_sheet->getStyle('A'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('B'.($nextt))->applyFromArray($style_ncentr);
$active_sheet->getStyle('C'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('D'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('E'.($nextt))->applyFromArray($style_ncentr);

	$active_sheet->setCellValueByColumnAndRow(0, $nextt, $nt);//nn
	$active_sheet->setCellValueByColumnAndRow(1, $nextt, $tovrow['pagetitle']);//Назва 
	$active_sheet->getCell('B'.$nextt)->getHyperlink()->setUrl($tovlink);//link
	$active_sheet->setCellValueByColumnAndRow(2, $nextt, $vimir);//Од.вим.
	$active_sheet->setCellValueByColumnAndRow(3, $nextt, $col);//Кіль
	$active_sheet->setCellValueByColumnAndRow(4, $nextt, strip_tags(html_entity_decode($compl)));//compl
	
	$active_sheet->getStyle('E'.($nextt))->getAlignment()->setWrapText(true);

//$nextt1 = $nextt+1;
	}
			}//pcat
		//++++
		
	}
			}

			}
		} else { 
		//if($categoryrow['template'] == 5) {
$selecttv = $modx->db->query("SELECT contentid, value FROM modx_site_tmplvar_contentvalues WHERE tmplvarid = 30".$scid2);
//$rezultc1 = $modx->db->query("SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE tmplvarid='27'");
if($modx->db->getRecordCount($selecttv) >= 1 ) {
				foreach($selecttv as $rowc1) {
	$search = stristr($rowc1['value'], '||');
	if(!empty($search)) {
		$arr = explode("||", $rowc1['value']);
			foreach($arr as $val) {
				if($val==$categoryrow['id']) {
								foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1['contentid']) and ($piec3[1]==$val)) {
												$tovtvs = $rowc1['contentid'];
												$tovtvsll[$categoryrow['id']] .= $tovtvs.',';
									} 
								}//	
				} 
			} 
	} else {
		if($rowc1['value']==$categoryrow['id']) {
			foreach($tovarpcat as $onepc){//
								$piec3 = explode("||", $onepc);
									if(($piec3[0]==$rowc1['contentid']) and ($piec3[1]==$rowc1['value'])) {
					$tovtvs = $rowc1['contentid'];
					$tovtvsll[$categoryrow['id']] .= $tovtvs.',';
									} 
		} // 
		}  
	} 
					unset($tovtvs);

}

}
$selectparent = $modx->db->select("id,parent", "modx_site_content", $scid1." parent='".$categoryrow['id']."' AND published != '0'");
if($modx->db->getRecordCount($selectparent) > 0 ) {
    while($tovparent = $modx->db->getRow($selectparent)) {
		foreach($tovarpcat as $onepc1){//

			$piec31 = explode("||", $onepc1);
				if(($piec31[0]==$tovparent['id']) and ($piec31[1]==$tovparent['parent'])) {
						$tovtvs1 = $tovparent['id'].',';
				} else { $tovtvs1 ="";  
					   }
			$tovtvsll[$categoryrow['id']] .= $tovtvs1;
		}//	
	}
}
	$tovtvsll[$categoryrow['id']] = substr($tovtvsll[$categoryrow['id']], 0, -1);
	if(!empty($tovtvsll[$categoryrow['id']])) {
	$scid4 = " id IN (".$tovtvsll[$categoryrow['id']].") AND ";
	} //else { $scid4 = " id IN (".$scid1.") ";
	//$scid4 = " parent='".$categoryrow['id']."' "; 
	//}
$tovar = $modx->db->select("id,parent,alias,pagetitle", "modx_site_content", $scid4." published != '0'");
         if($modx->db->getRecordCount($tovar) >= 1 ) {
			 $nt=0;
    while($tovrow = $modx->db->getRow($tovar)) {//1 uroven
$in++;
$nt++;
		//if($intt>1) { $in=$in+1; $in=$in-1; } else { $in=$in; }
$nextt = 7 + $in;
$nextt1 = $nextt+1;////
$nextt2 = $nextp-1;
	foreach($idtt as $ones){
$piecess = explode("||", $ones);
$idtv1=$piecess[0];
$colid=$piecess[1];
		if($idtv1==$tovrow['id']){
			$col=$colid;
		} 
	}
$idvl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'tovatatr'));//tovact		
		$tovlink="https://b-pro.com.ua/katalog/".$categoryrow['alias']."/".$pcategoryrow11['alias']."/".$pcategoryrow11['alias']."/".$tovrow['alias'];
$vimir  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'vimir'));
if(empty($vimir)) { $vimir='шт'; }
$price  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'price'));
		if(empty($price) or $price=='цену уточняйте') { $price='0'; } else { $price=$price;  }
$compl  = $modx->runSnippet("DocInfo", array('docid' => $tovrow['id'], 'field' => 'opistz'));
	$active_sheet->getStyle('B'.($nextt))->getAlignment()->setWrapText(true);
$active_sheet->getStyle('A'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('B'.($nextt))->applyFromArray($style_ncentr);
$active_sheet->getStyle('C'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('D'.($nextt))->applyFromArray($style_centr);
$active_sheet->getStyle('E'.($nextt))->applyFromArray($style_ncentr);

	$active_sheet->setCellValueByColumnAndRow(0, $nextt, $nt);//nn
	$active_sheet->setCellValueByColumnAndRow(1, $nextt, $tovrow['pagetitle']);//Назва 
	$active_sheet->getCell('B'.$nextt)->getHyperlink()->setUrl($tovlink);//link
	$active_sheet->setCellValueByColumnAndRow(2, $nextt, $vimir);//Од.вим.
	$active_sheet->setCellValueByColumnAndRow(3, $nextt, $col);//Кіль
	$active_sheet->setCellValueByColumnAndRow(4, $nextt, strip_tags(html_entity_decode($compl)));//compl
	
	$active_sheet->getStyle('E'.($nextt))->getAlignment()->setWrapText(true);

//$nextt1 = $nextt+1;
	}

			}//pcat
		}//5 в главном разделе	
	}
	
}
$nextt12=$nextt+3;
$nextt122=$nextt12+1;
$nextt123=$nextt122+1;
$nextt121=$nextt122+106;
$active_sheet->mergeCells('A'.($nextt12).':E'.($nextt12));
$active_sheet->mergeCells('A'.($nextt122).':E'.($nextt121));
$active_sheet->getStyle('A'.($nextt122))->getAlignment()->setWrapText(true);
$active_sheet->getRowDimension($nextt12)->setRowHeight(22);
$active_sheet->getStyle('B'.($nextt122))->getAlignment()->setWrapText(true);
$active_sheet->getStyle('C'.($nextt122))->getAlignment()->setWrapText(true);
$active_sheet->getStyle('D'.($nextt122))->getAlignment()->setWrapText(true);
$active_sheet->getStyle('E'.($nextt122))->getAlignment()->setWrapText(true);
$active_sheet->getStyle('A'.($nextt122))->applyFromArray($style_teh);
	$active_sheet->setCellValueByColumnAndRow(0, $nextt12, 'Вимоги до предмету закупівлі');
//$active_sheet->getStyle('A'.($nextt12)->applyFromArray($style_tehz);
$active_sheet->getStyle('A'.($nextt12))->applyFromArray($style_tehz);
$active_sheet->getStyle('A'.($nextt122))->applyFromArray($style_rtext);
	$active_sheet->setCellValueByColumnAndRow(0, $nextt122, 'Вимоги до предмету закупівлі
1. Технічні, якісні характеристики Товару за предметом закупівлі повинні відповідати встановленим/зареєстрованим діючим нормативним актам діючого законодавства (державним стандартам), які передбачають застосування заходів із захисту довкілля, охорони праці, екології та пожежної безпеки. '.chr(10).'
2. Товар повинен бути новим. '.chr(10).'
3. Гарантійний термін на поставлений товар повинен складати: не менше 12 місяців з дати поставки товару покупцю. '.chr(10).'
4. Упаковка повинна бути цілісна та непошкоджена, з необхідними реквізитами виробника. '.chr(10).'
5. Постачальник несе ризик за пошкодження або знищення Товару до моменту поставки його Покупцю. '.chr(10).'
6. З метою підтвердження відповідності товару, що поставляється,  технічним вимогам, Учасник повинен  надати в електронному вигляді (сканованому в форматі pdf.) в складі своєї пропозиції також наступні документи: '.chr(10).'
• порівняльну таблицю відповідності запропонованого товару технічним вимогам Замовника; '.chr(10).'
• якщо учасник процедури закупівлі не є виробником, для підтвердження статусу офіційного представника виробника необхідно надати лист авторизації виробника (або його офіційного  представника) на засоби навчання та обладнання навчального і загального призначення для кабінетів природничо – математичних предметів загальноосвітніх навчальних закладів та лист авторизації виробника (або його офіційного представника) мультимедійного обладнання; '.chr(10).'
• копію висновку державної санітарно-епідеміологічної експертизи на демонстраційне  обладнання; '.chr(10).'
• копію висновку державної санітарно-епідеміологічної експертизи на комплекти лабораторні; '.chr(10).'
• копію висновку державної санітарно-епідеміологічної експертизи на цифровій вимірювальний комплекс; '.chr(10).'
• сертифікати відповідності та паспорти на лабораторні комплекти закупівлі; '.chr(10).'
• свідоцтво про визнання відповідності педагогічним вимогам на засоби навчання та обладнання навчального і загального призначення для кабінетів приподничо – математичних предметів загальноосвітніх навчальних закладі;'.chr(10).'
• копію сертифікату відповідності технічному регламенту низковольтного електичного обладнання на Інтерактивний (мультимедійний) комплекс або на кожну з його складових'.chr(10).'
• -копію сертифікату відповідності технічному регламенту з електромагнітної сумісності на Інтерактивний (мультимедійний) комплекс або на кожну з його складових; '.chr(10).'
• копію сертифікату відповідності ДСТУ (або ТУ) на Інтерактивний (мультимедійний) комплекс або на кожну з його складових. '.chr(10).'
• копію чинного на дату проведення аукціону висновку державної санітарно-епідеміологічної експертизи Міністерства охорони здоров’я України на Цифровий вимірювальний комплекс; '.chr(10).'
• копію сертифікату відповідності технічному регламенту низьковольтного електричного обладнання на Цифровий вимірювальний комплекс; '.chr(10).'
• копії сертифікатів на Товар, що має бездротові комунікації (Wi-Fi, Bluetooth), які підтверджують відповідність запропонованого Товару вимогам технічних регламентів щодо електромагнітної сумісності, безпеки, радіо чинних на дату розкриття пропозицій; '.chr(10).'
• довідка про наявність обладнання та матеріально технічної бази; '.chr(10).'
• довідка про наявність працівників необхідної кваліфікації та досвіду роботи; '.chr(10).'
Учасники можуть додатково надавати у складі пропозицій інші документи та інформацію, які на їх думку, підтверджують відповідність пропозицій технічним, якісним та іншим характеристикам (вимогам) предмета закупівлі, передбаченим  у цьому Додатку. '.chr(10).'
Примітка: '.chr(10).'
1. Всі посилання на конкретну марку, виробника, фірму, патент, конструкцію або тип предмета закупівлі, джерело його походження або виробника, слід читати з виразом „або еквівалент”. Дане технічне завдання складене відповідно до Типового переліку засобів навчання та обладнання навчального і загального призначення для кабінетів природничо-математичних предметів загальноосвітніх навчальних закладів, затвердженого наказом Міністерства освіти і науки України від 22 червня 2016 року № 704 та Положення про навчальні кабінети загальноосвітніх навчальних закладів, затвердженого Наказом Міністерства освіти і науки України від 20.07.2004 року №601. '.chr(10).'
2. Учасник, прпозиція якого оцінююється, повинен у термін 3-х робочих  днів  на вимогу Замовнику надати зразки запропонованого товару у складі пропозиції та методичні рекомендації для проведення лабораторних та демонстраційних робіт з фізики у відповідності до вимог закупівлі та тендерної документації у випадку не виконання даної вимоги Замовника у часника буде дискваліфіковано. '.chr(10).'
3. При відсутності хоча б одного із вищезазначених документів в складі пропозиції до дати початку аукціону, пропозиція вважається такою, що не відповідає технічним вимогам закупівлі. 



 
');
	

$nmfile='tz.xls';
}
else {
	$active_sheet->mergeCells('A1:F1');
		//$active_sheet->setCellValue('B1', 'E-mail: info@b-pro.com.ua '.chr(10).'Сайт: https://b-pro.com.ua');
		$active_sheet->setCellValue('A1', 'Вы ничегоне выбрали. Вернитесь и повторите выбор товара');
		$nmfile='error.xlsx';

}
	
  
        //Сохраняем файл с помощью PHPExcel_IOFactory и указываем тип Excel
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        
        //Отправляем заголовки с типом контекста и именем файла
        header('Content-Type:application/vnd.ms-excel');
        header('Content-Disposition:attachment;filename="'.$nmfile.'"');

        ob_end_clean(); 
            
        //Отправляем файл
        $objWriter->save('php://output');
        
		//$objWriter = new PHPExcel_Writer_Excel2007($pExcel);
//$objWriter->save('php://output');
        /* удаление выборки */
        $result->free();

        /* закрытие соединения */
        //$mysqli->close();
