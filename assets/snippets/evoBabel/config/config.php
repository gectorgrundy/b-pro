<?php
//@author webber (web-ber12@yandex.ru)

if (!defined('MODX_BASE_PATH')) {
    die('What are you doing? Get out of here!');
}

//активный язык отдельно от списка
$activeLang = '<li id="curr_lang"><a href="javascript:;">[+alias+]</a></li>'; //<img src="assets/site/images/[+alias+].png">

//активный язык в списке
$activeRow = '<li class="active">&nbsp;<a class="active" href="[+url+]">[+alias+]</a></li>'; //<img src="assets/site/images/[+alias+].png">

//неактивный язык списка
$unactiveRow = '<li>&nbsp;<a href="[+url+]">[+alias+]</a></li>'; //<img src="assets/site/images/[+alias+].png">

//обертка списка языков
$langOuter = '<ul class="other_langs language" class="">[+wrapper+]</ul>';
