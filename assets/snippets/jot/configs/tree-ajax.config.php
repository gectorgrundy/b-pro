<?php
	setlocale (LC_ALL, 'ru_RU.UTF-8');
	$customfields = isset($customfields) ? $customfields : 'name,email';
	$validate = isset($validate) ? $validate : 'name:Вы не написали своё имя,email:Неправильный e-mail:email,content:Вы не заполнили поле комментария';
	$action = isset($action) ? $action : 'tree';
	$sortby = isset($sortby) ? $sortby : 'createdon:a';
	$cssFile = isset($cssFile) ? $cssFile : 'assets/snippets/jot/css/tree.css';
	$js = isset($js) ? $js : 1;
	$jsFile = isset($jsFile) ? $jsFile : 'assets/snippets/jot/js/tree.js';

	$onDeleteComment = isset($onDeleteComment) ? $onDeleteComment : 'delthread';
	
	$onBeforePOSTProcess = isset($onBeforePOSTProcess) ? $onBeforePOSTProcess : 'antispam';
	$onSetFormOutput = isset($onSetFormOutput) ? $onSetFormOutput : 'antispam,ajax';
	$onBeforeValidateFormField = isset($onBeforeValidateFormField) ? $onBeforeValidateFormField : 'nolink';

	$onBeforeFirstRun = isset($onBeforeFirstRun) ? $onBeforeFirstRun : 'subscribe';
	$onFirstRun = isset($onFirstRun) ? $onFirstRun : 'rating';
	$onSaveComment = isset($onSaveComment) ? $onSaveComment : 'subscribe';
	$onBeforeRunActions = isset($onBeforeRunActions) ? $onBeforeRunActions : 'subscribe';
	$onBeforeProcessPassiveActions = isset($onBeforeProcessPassiveActions) ? $onBeforeProcessPassiveActions : 'subscribe';
	$onGetSubscriptions = isset($onGetSubscriptions) ? $onGetSubscriptions : 'subscribe';
	$onBeforeGetUserInfo = isset($onBeforeGetUserInfo) ? $onBeforeGetUserInfo : 'subscribe';
	$onBeforeNotify = isset($onBeforeNotify) ? $onBeforeNotify : 'subscribe';
	
	$onSetCommentsOutput = isset($onSetCommentsOutput) ? $onSetCommentsOutput : 'ajax';
	$onReturnOutput = isset($onReturnOutput) ? $onReturnOutput : 'rating,ajax';
	
if (strstr($_SERVER['REQUEST_URI'], '?')){
	$url=strtok($_SERVER['REQUEST_URI'], '?');
}else{
	$url=$_SERVER['REQUEST_URI'];
}

if (mb_ereg( '/ru/', $url ) )
{
	$leave_feedback = 'Оставить отзыв';
	$edit_comment = 'Изменить комментарий';
	$leave_your_feedback = 'Оставте свой отзыв';
	$message_3 = 'Вы пытаетесь отправить одно и то же сообщение. Возможно вы нажали кнопку отправки более одного раза.';
	$message_2 = 'Ваше сообщение было отклонено.';
	$message_1 = 'Ваше сообщение сохранено, оно будет опубликовано после просмотра администратором.';
	$message1 = 'Вы пытаетесь отправить одно и то же сообщение. Возможно вы нажали кнопку отправки более одного раза.';
	$message2 = 'Вы ввели неправильный защитный код.';
	$message3 = 'Вы можете отправлять сообщения не чаще';
	$message4 = 'Ваше сообщение было отклонено.';
	$message5 = 'Вы не заполнили все требуемые поля';
	$confirm1 = 'Ваше сообщение опубликовано.';
	$confirm2 = 'Ваше сообщение сохранёно, оно будет опубликовано после просмотра администратором.';
	$confirm3 = 'Сообщение сохранено.';
	$confirm4 = 'Вы отписались от уведомлений.';
	$your_name = 'Ваше имя';
	$ocenka = 'Оцените товар';
	$enter_comment = 'Введите комментарий';
	$save = 'Сохранить';
	$send = 'Отправить';
}else{
	$leave_feedback = 'Залишити відгук';
	$edit_comment = 'Змінити коментар';
	$leave_your_feedback = 'Залиште свій відгук';
	$message_3 = 'Ви намагаєтеся відправити одне і те ж повідомлення. Можливо ви натиснули кнопку відправки більше одного разу.';
	$message_2 = 'Ваше повідомлення було відхилено.';
	$message_1 = 'Ваше повідомлення збережено, воно буде опубліковано після перегляду адміністратором.';
	$message1 = 'Ви намагаєтеся відправити одне і те ж повідомлення. Можливо ви натиснули кнопку відправки більше одного разу.';
	$message2 = 'Ви ввели неправильний код захисту.';
	$message3 = 'Ви можете відправляти повідомлення не частіше';
	$message4 = 'Ваше повідомлення було відхилено.';
	$message5 = 'Ви не заповнили всі необхідні поля';
	$confirm1 = 'Ваше повідомлення опубліковано.';
	$confirm2 = 'Ваше повідомлення збережені, воно буде опубліковано після перегляду адміністратором.';
	$confirm3 = 'Повідомлення збережено.';
	$confirm4 = 'Ви відписалися від повідомлень.';
	$your_name = 'Ваше ім’я';
	$ocenka = 'Оцiнити товар';
	$enter_comment = 'Введіть коментар';
	$save = 'Зберегти';
	$send = 'Надіслати';
}

	$tplForm = '@CODE:
<div class="jot-form-container">
<div class="jot-show-form">'.$leave_feedback.'</div>
<div class="jot-form-block">
<div id="respond-[+jot.link.id+]" class="jot-form-wrap">
<a name="jf[+jot.link.id+]"></a>
<h3 class="jot-reply-title"><a class="jot-btn jot-reply-cancel" href="[~[*id*]~]#jf[+jot.link.id+]" id="cancel-comment-link-[+jot.link.id+]" rel="nofollow">Отменить</a>[+form.edit:is=`1`:then=`'.$edit_comment.'`:else=`'.$leave_your_feedback.'`+]</h3>
<script type="text/javascript">document.getElementById("cancel-comment-link-[+jot.link.id+]").style.display = "none"</script>
[+form.error:isnt=`0`:then=`
<div class="jot-err">
[+form.error:select=`
&-3='.$message_3.'
&-2='.$message_2.'
&-1='.$message_1.'
&1='.$message1.'
&2='.$message2.'
&3='.$message3.' [+jot.postdelay+] секунд.
&4='.$message4.'
&5=[+form.errormsg:ifempty=`'.$message5.'`+]
`+]
</div>
`:strip+]
[+form.confirm:isnt=`0`:then=`
<div class="jot-cfm">
[+form.confirm:select=`
&1='.$confirm1.'
&2='.$confirm2.'
&3='.$confirm3.'
&4='.$confirm4.'
`+]
</div>
`:strip+]
<form method="post" action="[+form.action:esc+]#jf[+jot.link.id+]" class="jot-form">
	<input name="JotForm" type="hidden" value="[+jot.id+]" />
	<input name="JotNow" type="hidden" value="[+jot.seed+]" />
	<input name="parent" type="hidden" value="[+form.field.parent+]" id="comment-parent-[+jot.link.id+]" />
	
	[+form.moderation:is=`1`:then=`
	<div class="jot-info">
		<b>Создан:</b> [+form.field.createdon:date=`%d.%m.%Y в %H:%M`+]<br />
		<b>Автор:</b> [+form.field.createdby:userinfo=`username`:ifempty=`[+jot.guestname+]`+]<br />
		<b>IP:</b> [+form.field.secip+]<br />
		<b>Опубликовано:</b> [+form.field.published:select=`0=Нет&1=Да`+]<br />
		[+form.field.publishedon:gt=`0`:then=`
		<b>Дата публикации:</b> [+form.field.publishedon:date=`%d.%m.%Y в %H:%M`+]<br />
		<b>Опубликовал:</b> [+form.field.publishedby:userinfo=`username`:ifempty=` - `+]<br />
		`+]
		[+form.field.editedon:gt=`0`:then=`
		<b>Дата изменения:</b> [+form.field.editedon:date=`%d.%m.%Y в %H:%M`+]<br />
		<b>Редактировал:</b> [+form.field.editedby:userinfo=`username`:ifempty=` -`+]<br />
		`+]
	</div>
	`:strip+]
	
		[+form.guest:is=`1`:then=`
                <div class="jot-controls">
<p>
	'.$ocenka.'
</p>
                        <div class="jot-input-prepend">
                                <input name="zvezda" id="rating" value="1">
 <div class="rating">
  <div class="rating-item active" data-rate="1"></div>
  <div class="rating-item" data-rate="2"></div>
  <div class="rating-item" data-rate="3"></div>
  <div class="rating-item" data-rate="4"></div>
  <div class="rating-item" data-rate="5"></div>
</div>                        

                        </div>
                </div>
        `+]
	[+form.guest:is=`1`:then=`
	<div class="jot-controls">
		<span>
			'.$your_name.'
		</span>
		<div class="jot-input-prepend">
			<span class="jot-add-on"><i class="jot-icon-user"></i></span><input tabindex="[+jot.seed:math=`?+1`+]" name="name" type="text" size="40" value="[+form.field.custom.name:esc+]" placeholder="'.$your_name.'" title="'.$your_name.'" />
		</div>
	</div>
	`+]
	<div class="jot-controls">
		<span>
			'.$enter_comment.'
		</span>
		<textarea tabindex="[+jot.seed:math=`?+4`+]" name="content" cols="50" rows="6" id="content-[+jot.link.id+]" placeholder="'.$enter_comment.'...">[+form.field.content:esc+]</textarea>
	</div>
	<div class="jot-form-actions">
		<button tabindex="[+jot.seed:math=`?+5`+]" class="jot-btn jot-btn-submit btn btn-blue" type="submit">[+form.edit:is=`1`:then=`'.$save.'`:else=`'.$send.'`+]</button>
		[+form.edit:is=`1`:then=`
		<button tabindex="[+jot.seed:math=`?+6`+]" class="jot-btn jot-btn-cancel" onclick="history.go(-1);return false;">[%cancel%]</button>
		`+]
	</div>
</form>
</div>
</div>
</div>
	';

	$tplComments = '@CODE:
<div class="jot-comment">
	<a name="jc[+jot.link.id+][+comment.id+]"></a>
	<div class="jot-row [+chunk.rowclass+] [+comment.published:is=`0`:then=`jot-row-up`+]">
		<div class="jot-comment-head">
			<div class="jot-mod">
				[+jot.user.canedit:is=`1`:and:if=`[+comment.createdby+]`:is=`[+jot.user.id+]`:or:if=`[+jot.moderation.enabled+]`:is=`1`:then=`
					<a class="jot-btn jot-btn-edit" href="[+jot.link.edit:esc+][+jot.querykey.id+]=[+comment.id+]#jf[+jot.link.id+]" title="Изменить"><i class="jot-icon-edit"></i> Изменить</a>
				`:strip+]
				[+jot.moderation.enabled:is=`1`:then=`
					[+comment.published:is=`0`:then=`<a class="jot-btn jot-btn-pub" href="[+jot.link.publish:esc+][+jot.querykey.id+]=[+comment.id+]#jotmod[+jot.link.id+]" title="Показать"><i class="jot-icon-pub"></i> Показать</a>`+]
					[+comment.published:is=`1`:then=`<a class="jot-btn jot-btn-unpub" href="[+jot.link.unpublish:esc+][+jot.querykey.id+]=[+comment.id+]#jotmod[+jot.link.id+]" title="Скрыть"><i class="jot-icon-unpub"></i> Скрыть</a>`+]
					<a class="jot-btn jot-btn-del" href="[+jot.link.delete:esc+][+jot.querykey.id+]=[+comment.id+]#jotmod[+jot.link.id+]" onclick="return confirm(\'Вы действительно хотите удалить это сообщение?\')" title="Удалить"><i class="jot-icon-del"></i> Удалить</a>
				`:strip+]
			</div>
			<span class="jot-name">[+comment.username:ifempty=`[+comment.custom.name:ifempty=`[+jot.guestname+]`:esc+]`+] [+jot.moderation.enabled:is=`1`:then=`<span class="jot-extra"><a target="_blank" href="http://www.ripe.net/perl/whois?searchtext=[+comment.secip+]">([+comment.secip+])</a></span>`+]</span>
			<div class="myraiting myraiting[+comment.custom.zvezda+]"><span class="star1"></span><span class="star2"></span><span class="star3"></span><span class="star4"></span><span class="star5"></span></div>
		</div>
		<div class="jot-comment-entry" id="comment-[+jot.link.id+]-[+comment.id+]">
			<div class="jot-message">[+comment.content:wordwrap:esc:nl2br+]</div>
		</div>
	</div>
	<div class="jot-children">
		[+jot.wrapper+]
	</div>
</div>
	';
	
	$tplNav = '@CODE:
<a name="jotnav[+jot.link.id+]"></a>
<div class="jot-nav">
	<a rel="nofollow" class="jot-btn jot-show-all" href="[+jot.link.navigation:esc+][+jot.querykey.navigation+]=0#jotnav[+jot.link.id+]">Просмотреть все</a>
	[+jot.page.current:gt=`1`:then=`
	<a rel="nofollow" class="jot-btn" href="[+jot.link.navigation:esc+][+jot.querykey.navigation+]=[+jot.page.current:math=`?-1`+]#jotnav[+jot.link.id+]">&laquo; Предыдущяя</a>
	`+]
	[+jot.pages+]
	[+jot.page.current:lt=`[+jot.page.total+]`:then=`
	<a rel="nofollow" class="jot-btn" href="[+jot.link.navigation:esc+][+jot.querykey.navigation+]=[+jot.page.current:math=`?+1`+]#jotnav[+jot.link.id+]">Следующая &raquo;</a>
	`+]
</div>
	';
	
	$tplNavPage = '@CODE:
	<a rel="nofollow" class="jot-btn" href="[+jot.link.navigation:esc+][+jot.querykey.navigation+]=[+jot.page.num+]#jotnav[+jot.link.id+]">[+jot.page.num+]</a>
	';
	
	$tplNavPageCur = '@CODE:
	<a rel="nofollow" class="jot-btn jot-btn-active" href="[+jot.link.navigation:esc+][+jot.querykey.navigation+]=[+jot.page.num+]#jotnav[+jot.link.id+]">[+jot.page.num+]</a>
	';

?>