/*LiveAjaxMegaSearch*/
var $ = jQuery.noConflict();

$(document).ready(function() {
	$('html').attr('id', 'js'); // Enabled Javascript Detection

	if($("a[rel=external]")){
		$("a[rel=external]").click(function() {
			window.open(this.href);  
			return false;
		});
	}

	if($(".nav_serch_form input[type=text]")){
		$(".nav_serch_form input[type=text]").autoClear();
	}

	$(".have_sub a:first").click(function(){
		$(this).next("ul").slideToggle("slow");
		$(this).toggleClass("active");
		$(this).parent("li").toggleClass("active");
	 });

	if($(".home_catalog ul")){
		 $(".home_catalog li:odd").addClass('odd');
	}
	
	$(".fancy_video").click(function() {
		$.fancybox({
			'padding'		: 0,
			'autoScale'		: false,
			'transitionIn'	: 'none',
			'transitionOut'	: 'none',
			'title'			: this.title,
			'width'			: 640,
			'height'		: 385,
			'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
			'type'			: 'swf',
			'swf'			: {
			'wmode'				: 'transparent',
			'allowfullscreen'	: 'true'
			}
		});

		return false;
	});
	
	/*search-result show hide on mause enter or leave*/
    var count = 0;
    $('#search-result, .nav_serch_form').mouseenter(function() {
        count++;
		if($('.search_item').length > 0){
			$('#search-result').show();
		}
    });

    $('#search-result, .nav_serch_form').mouseleave(function() {
        count--;
        if (count == 0){
            $('#search-result').hide();
        }
    });
	
	$(document).on('click', '#search-result', function() {$('#search-result').hide();})
	
	/*LiveAjaxMegaSearch*/
	function LiveAjaxMegaSearch(as) {
		if (as['minlength'] > as['text'].length) {
			$('#search-result .search-inner').empty();
			$('#search-result').hide();
			return false;
		}

		$.ajax({
			url: 'ajaxmegasearch',
			type: 'post',
			data: {
				text: as['text'],
				minlength: as['minlength'],
				fields: as['fields']
			},
			success: function(data) {
				if (data) {
					$('#search-result .search-inner').html(data);
					$('#search-result').show();
				}
			}
		});

	}
	var as = [];
	as['minlength'] = 2;
	as['fields'] = 'pagetitle,longtitle,menutitle,tv.value';
	as['text'] = $('input#search').val();
	$('#ajaxSearch_input').keyup(function() {
			as['text'] = $(this).val();
			LiveAjaxMegaSearch(as);
	});
	
});

(function($) {
    $.fn.autoClear = function () {
        $(this).each(function() {
            $(this).data("autoclear", $(this).attr("value"));
        });
        $(this)
            .bind('focus', function() {   
                if ($(this).attr("value") == $(this).data("autoclear")) {
                    $(this).attr("value", "").addClass('autoclear-normalcolor');
                }
            })
            .bind('blur', function() {  
                if ($(this).attr("value") == "") {
                    $(this).attr("value", $(this).data("autoclear")).removeClass('autoclear-normalcolor');
                }
            });
        return $(this);
    }
})(jQuery)