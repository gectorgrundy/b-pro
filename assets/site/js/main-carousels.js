jQuery(document).ready(function($) {	
	jQuery('.review-carousel').each( function() {
		jQuery(this).owlCarousel({
			items:1,dots: true,nav: true,navText: false,loop: true,autoplay:false,margin: 0,autoHeight: true
		});
	});
	jQuery('.main-slider2').each( function() {
		jQuery(this).owlCarousel({
			items:1,dots:true,nav:true,navText:false,loop:true,autoplay:true,animateOut: 'fadeOut'
		});
	});
});	