<!-- START FOOTER -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-12 col-xl-5 col-lg-5 col-md-12">
          <div class="bottom-nav item">
            <p class="title">
              Каталог товаров
            </p>
            <ul>
              <li>
                <a href="#">
                  Готовые наборы
                </a>
              </li>
              <li>
                <a href="#">
                  Защита отечества
                </a>
              </li>
              <li>
                <a href="#">
                  Начальная школа
                </a>
              </li>
              <li>
                <a href="#">
                  Физика
                </a>
              </li>
              <li>
                <a href="#">
                  Химия
                </a>
              </li>
              <li>
                <a href="#">
                  География
                </a>
              </li>
              <li>
                <a href="#">
                  Биология
                </a>
              </li>
              <li>
                <a href="#">
                  Приказы МОН Украины
                </a>
              </li>
            </ul>
            <ul>
              <li>
                <a href="#">
                  Математика
                </a>
              </li>
              <li>
                <a href="#">
                  Уроки труда
                </a>
              </li>
              <li>
                <a href="#">
                  Инклюзивновное образование
                </a>
              </li>
              <li>
                <a href="#">
                  Компьютерная техника
                </a>
              </li>
              <li>
                <a href="#">
                  Презентационное оборудование
                </a>
              </li>
              <li>
                <a href="#">
                  Мебель
                </a>
              </li>
              <li>
                <a href="#">
                  Измирительные комплексы Vernier
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-6 col-xl-2 col-lg-2 col-md-6">
          <div class="bottom-nav">
            <p class="title">
              Меню
            </p>
            <ul>
              <li>
                <a href="#">
                  Главная
                </a>
              </li>
              <li>
                <a href="#">
                  О нас
                </a>
              </li>
              <li>
                <a href="#">
                  Наши проекты
                </a>
              </li>
              <li>
                <a href="#">
                  Оплата и доставка
                </a>
              </li>
              <li>
                <a href="#">
                  Возврат
                </a>
              </li>
              <li>
                <a href="#">
                  Блог
                </a>
              </li>
              <li>
                <a href="#">
                  Контакты
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-6 col-xl-2 col-lg-2 col-md-6">
          <div class="bottom-nav">
            <p class="title">
              Наш адрес:
            </p>
            <p class="address">
              04074, м. Київ,
              вул. Новозабарська 2/6,
              а/с 228.
            </p>
            <p class="title">
              Обратная связь:
            </p>
            <div class="phone">
              Тел.: 
              <a href="#">
                (044) 353-33-77
              </a>
            </div>
            <p class="title">
              Мы в соц. сетях:
            </p>
            <div class="social">
              <a href="#">
                <img src="img/icons/facebook.svg" alt="">
              </a>
              <a href="#">
                <img src="img/icons/youtube.svg" alt="">
              </a>
            </div>
          </div>
        </div>
        <div class="col-12 col-xl-3 col-lg-3 col-md-12">
          <div class="bottom-info">
            <a href="#">
              <img src="img/footer-logo.png" alt="">
            </a>
            <p class="copyright">
              2019 B-Pro All rights reserved
            </p>
            <div class="cards">
              <img src="img/mastercard-logo.png" alt="">
              <img src="img/visa-logo.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
<!-- END FOOTER -->

<a href="#body" class="btn-top anchor">
  <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-right fa-w-8 fa-2x"><path fill="currentColor" d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class=""></path> 
  </svg>
</a>

<!-- START MODAL -->
  <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <button type="button" class="close-modal" data-dismiss="modal" aria-label="Close">
            <img src="img/icons/close-button.svg" alt="">
          </button>
          <div class="img">
            <img src="img/modal-img.png" alt="">
          </div>
          <div class="info-block">
            <p class="title">
              Скачайте актуальный прайс-лист всех товаров для кабинета физики
            </p>
            <p class="subtitle">
              Введите свои контакты и <br/> 
              мгновенно получите актуальный прайс-лист
            </p>
            <form method="post">
              <label>
                <span>
                  Ваше имя
                </span>
                <input type="text" class="form-element" required="" placeholder="Введите ваше имя">
              </label>
              <label>
                <span>
                  Ваш номер телефона
                </span>
                <input type="text" class="form-element" required="" placeholder="Введите номер телефона">
              </label>
              <button type="submit" class="form-btn btn btn-blue">
                Скачать, xls (5,12 Mb)
              </button>
            </form>
          </div>
        </div>
     </div>
  </div>
  <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <button type="button" class="close-modal" data-dismiss="modal" aria-label="Close">
            <img src="img/icons/close-button.svg" alt="">
          </button>
          <div class="img">
            <img src="img/modal-img.png" alt="">
          </div>
          <div class="info-block">
            <p class="title">
              Скачайте актуальный прайс-лист всех товаров
            </p>
            <p class="subtitle">
              Введите свои контакты и <br/> 
              мгновенно получите актуальный прайс-лист
            </p>
            <form method="post">
              <label>
                <span>
                  Ваше имя
                </span>
                <input type="text" class="form-element" required="" placeholder="Введите ваше имя">
              </label>
              <label>
                <span>
                  Ваш номер телефона
                </span>
                <input type="text" class="form-element" required="" placeholder="Введите номер телефона">
              </label>
              <button type="submit" class="form-btn btn btn-blue">
                Скачать, xls (5,12 Mb)
              </button>
            </form>
          </div>
        </div>
     </div>
  </div>
  <div class="modal modal-consultation fade" id="modal3" tabindex="-1" role="dialog" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <button type="button" class="close-modal" data-dismiss="modal" aria-label="Close">
            <img src="img/icons/close-button.svg" alt="">
          </button>
          <div class="info-block">
            <p class="title">
              Получите бесплатную консультацию <br/>
              по подготовке к тендеру
            </p>
            <p class="subtitle">
              Введите свои контакты<br/> 
              и наш менеджер свяжется с вами
            </p>
            <form method="post">
              <label>
                <span>
                  Ваше имя
                </span>
                <input type="text" class="form-element" required="" placeholder="Введите ваше имя">
              </label>
              <label>
                <span>
                  Ваш номер телефона
                </span>
                <input type="text" class="form-element" required="" placeholder="Введите номер телефона">
              </label>
              <button type="submit" class="form-btn btn btn-blue">
                Получить консультацию по тендеру
              </button>
            </form>
          </div>
        </div>
     </div>
  </div>
<!-- END MODAL -->
  <div class="overlay"></div>
  <script src="libs/jquery/jquery-2.2.4.min.js"></script>
  <script src="libs/formstyler/jquery.formstyler.min.js"></script>
  <script src="libs/popper/popper.min.js"></script>
  <script src="libs/bootstrap/bootstrap.min.js"></script>
  <script src="libs/slick/slick.js"></script>
  <script src="js/common.js?v23071500"></script>
  <script>
    $(document).ready(function() {
      var css = 
        '<style>\n\
          .markup-nav { cursor: pointer; position: fixed; z-index: 5000; top: 60px; left: -176px; width: 170px; font:bold 12px/1.2 Helvetica, Arial; box-shadow: 0 0 4px rgba(0, 0, 0, .6); transition: left 200ms ease; box-sizing: border-box; }\n\
          .markup-nav__inner { background:#fff; padding: 5px 10px 5px 0; position: relative; }\n\
          .markup-nav:before { content: ""; position: absolute; left: 100%; top: 0; width: 30px; height: 24px; background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAMAAABhEH5lAAAAwFBMVEUAAAD///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABiCivsAAAAP3RSTlMAAAIEEBEVFiYsLS4vMzg5Ojs8PT5BR0tUXV5gaG5vcHd4fby9v8DBwsTFycvMzc/g4+Tl5uvs7vP09fb5+v5kF+L8AAAAsklEQVR42m3QaReBQBTG8aks2VNIthARQostmvv9v5U7V5164f/qOb9zZuYUyzLi2KAhSTl5AF5B/cuhOYkAgpHqnTWiEwAH6gPgE+2h1JaozQtJVSIb59uu1lcpjjHSwH3gshi2xBG6QxYCliiCaiAKfvSUBVWIQqY54qApaIYj3mi41rhupqxYCY4pvdgD0UsA8AaRC6V2RD7k8fyDNN/tzO9436LlHLv/f06WHl31jL67eSl1BXfldgAAAABJRU5ErkJggg==) no-repeat 75% 50% white; box-shadow: 0 0 4px rgba(0, 0, 0, .6); }\n\
          .markup-nav a { display: block; padding: 2px 10px; color: #000; /*text-shadow: 1px 1px 0 #283E68;*/text-decoration: none !important; border: none !important; }\n\
          .markup-nav a:hover { background: #edf1f5; color: #000 !important; }\n\
          .markup-nav.is-open { left: 0; }\n\
        </style>';

      var html =
        '<div class="markup-nav">\n\
          <div class="markup-nav__inner">\n\
            <a href="index.php">Главная</a>\n\
            <a href="catalog.php">Каталог</a>\n\
            <a href="category.php">Категории</a>\n\
            <a href="product-inner.php">Карточка товара</a>\n\
          </div>\n\
        </div>'

      $('body').prepend(css + html);

      $('.markup-nav').on('click', function() {
        $(this).toggleClass('is-open');
      });

    });
  </script>
</body>
