$(document).ready(function(){


//$('[name=tel]').inputmask({"mask": "+38(999) 999-99-99"});

var im = new Inputmask("+38(999) 999-99-99",{ "clearIncomplete": true });
im.mask('[name=tel]');
im.mask('[name=phone]');

/*
$('[name=tel]').bind("change keyup input click", function() {
  if (this.value.match(/[^0-9]/g)) {
    this.value = this.value.replace(/[^0-9]/g, '');
  }
});
*/


  $('.mob-nav .basket .total').text($('.bottom-nav .basket .total').text());
  
  $(".seo .show_more_seo").click(function(){
    $('.seo .inner-seo').toggleClass('visible-block');
    $(this).closest('.show-more-btn').toggleClass('visited')
  });
  $('.main-slider').slick({
    slidesToShow: 1,
    centerMode: true,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    prevArrow: '<button type="button" class="slick-prev"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-left fa-w-8 fa-2x"><path fill="currentColor" d="M238.475 475.535l7.071-7.07c4.686-4.686 4.686-12.284 0-16.971L50.053 256 245.546 60.506c4.686-4.686 4.686-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.686-16.97 0L10.454 247.515c-4.686 4.686-4.686 12.284 0 16.971l211.051 211.05c4.686 4.686 12.284 4.686 16.97-.001z" class=""></path></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-right fa-w-8 fa-2x"><path fill="currentColor" d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class=""></path> </svg></button>',
    slidesToShow: 1,
    fade: true,
    infinite: true,
    cssEase: 'linear',
    draggable: false,
    autoplay: true,
    autoplaySpeed: 5000,
    adaptiveHeight: true
  });
  $('.about-us-slider').slick({
    infinite: true,
    arrows: true,
    dots: true,
    prevArrow: '<button type="button" class="slick-prev"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-left fa-w-8 fa-2x"><path fill="currentColor" d="M238.475 475.535l7.071-7.07c4.686-4.686 4.686-12.284 0-16.971L50.053 256 245.546 60.506c4.686-4.686 4.686-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.686-16.97 0L10.454 247.515c-4.686 4.686-4.686 12.284 0 16.971l211.051 211.05c4.686 4.686 12.284 4.686 16.97-.001z" class=""></path></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-right fa-w-8 fa-2x"><path fill="currentColor" d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class=""></path> </svg></button>',
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
  });
  $('.review-slider').slick({
    infinite: true,
    arrows: true,
    dots: false,
    dots: true,
    prevArrow: '<button type="button" class="slick-prev"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-left fa-w-8 fa-2x"><path fill="currentColor" d="M238.475 475.535l7.071-7.07c4.686-4.686 4.686-12.284 0-16.971L50.053 256 245.546 60.506c4.686-4.686 4.686-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.686-16.97 0L10.454 247.515c-4.686 4.686-4.686 12.284 0 16.971l211.051 211.05c4.686 4.686 12.284 4.686 16.97-.001z" class=""></path></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-right fa-w-8 fa-2x"><path fill="currentColor" d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class=""></path> </svg></button>',
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    adaptiveHeight: true
  });
  $('.proect__slider').slick({
    infinite: true,
    arrows: true,
    draggable: false,
    dots: false,
    prevArrow: '<button type="button" class="slick-prev"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-left fa-w-8 fa-2x"><path fill="currentColor" d="M238.475 475.535l7.071-7.07c4.686-4.686 4.686-12.284 0-16.971L50.053 256 245.546 60.506c4.686-4.686 4.686-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.686-16.97 0L10.454 247.515c-4.686 4.686-4.686 12.284 0 16.971l211.051 211.05c4.686 4.686 12.284 4.686 16.97-.001z" class=""></path></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-right fa-w-8 fa-2x"><path fill="currentColor" d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class=""></path> </svg></button>',
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true
  });
  $('.product-inner-slider').slick({
    infinite: true,
    arrows: true,
    dots: false,
    prevArrow: '<button type="button" class="slick-prev"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-left fa-w-8 fa-2x"><path fill="currentColor" d="M238.475 475.535l7.071-7.07c4.686-4.686 4.686-12.284 0-16.971L50.053 256 245.546 60.506c4.686-4.686 4.686-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.686-16.97 0L10.454 247.515c-4.686 4.686-4.686 12.284 0 16.971l211.051 211.05c4.686 4.686 12.284 4.686 16.97-.001z" class=""></path></svg></button>',
    nextArrow: '<button type="button" class="slick-next"><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-chevron-right fa-w-8 fa-2x"><path fill="currentColor" d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class=""></path> </svg></button>',
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 5000,
    asNavFor: '.slider-nav-thumbnails'
  });
  $('.slider-nav-thumbnails').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.product-inner-slider',
    dots: false,
    centerMode: false,
    vertical: true,
    focusOnSelect: true
  });
/*   $(".catalog-nav .catalog-menu li.has-child").on("mouseenter", function() {
    if($(window).width()>767) {
      $('body').addClass("catalog-opened");
    }
  }).on("mouseleave", function() {
    if($(window).width()>767) {
      $('body').removeClass("catalog-opened");
    }
  }); */

$('.catalog-nav .catalog-menu li').each(function(){
	$(this).on("mouseover", function() {
		if($(window).width()>767) {
			$this = $(this);
			$('body').addClass("catalog-opened");
			$this.find('.subitem').stop(true).show();
			$('.catalog-nav .catalog-menu li.has-child').removeClass('current');
			if($this.hasClass('has-child') && !$this.hasClass('current')){
				setTimeout(function() {
					$this.addClass('current');
				}, 1100);
			}
		}	
	}).on("mouseout", function() {
		if($(window).width()>767) {
			$('.catalog-nav .catalog-menu li .subitem').delay(1000).fadeOut(1);
		}
	});
});	
  
/*   
   $('.catalog-nav .catalog-menu li').on("mouseenter", function() {
    if($(window).width()>767) {
      $('body').addClass("catalog-opened");
	  //$('.catalog-nav .catalog-menu li.has-child').removeClass('current');
	
		$('.catalog-nav .catalog-menu li').removeClass('current');
	  
		$(this).addClass('current');
	
    }
  }); */
  $(".catalog-nav").on("mouseenter", function() {
    if($(window).width()>767) {
      $('.top-elements .hover-block').show();
    }
  })
  $(document).on('click','.catalog-opened',function(){
	  $('body').removeClass("catalog-opened");
	  $('.top-elements .hover-block').hide();
	  $('.catalog-nav .catalog-menu li.has-child').removeClass('current');
  });
  $('.catalog-block .catalog-elem.has-menu .title').click(function(){
    if($(window).width()<767) {
      $(this).toggleClass('active');
      $(this).closest('.catalog-elem.has-menu').find('ul').slideToggle();
      return false;
    }
  });
  $('.category-content .sidebar .title').click(function(){
    if($(window).width()<767){
      $(this).toggleClass('active');
      $('.category-content .sidebar .sidebar-menu').slideToggle();
    }
  });
  $('.catalog-nav .catalog-menu li .catalog-elem').click(function(){
    if($(window).width()<767) {
      if( $(this).closest('li').find('.subitem').length ){
        return false;
       }
    }
  });
  $('.catalog-nav .catalog-menu li.has-child a').click(function(){
    if($(window).width()<767) {
      
        var titleText = $(this).text();
        var titleImage = $(this).children('img').attr('src');
        $(this).next('.subitem').addClass('active');
        $('.catalog-nav .subitem .mob-top-elem .title span').html(titleText);
        $('.catalog-nav .subitem .mob-top-elem .title img').attr('src' , titleImage);
      
    }
  });

  $('.catalog-nav .mob-top-elem.sub-elem .back').click(function(){
    if($(window).width()<767) {
      $('.catalog-nav .catalog-menu .subitem').removeClass('active');
    }
  });
  $('.styler').styler({
    selectSearch: true
  });
  $('.category-content .product-nav-block .product-type-nav .view1').click(function(){
    $('.category-content .product-nav-block .product-type-nav .view').removeClass('active');
    $(this).addClass('active');
    $('.product-block').removeClass('product-view2');
  });
  $('.category-content .product-nav-block .product-type-nav .view2').click(function(){
    $('.category-content .product-nav-block .product-type-nav .view').removeClass('active');
    $(this).addClass('active');
    $('.product-block').addClass('product-view2');
  });
  $('.product-inner .product-review .review-block .read-more').click(function(){
    $('.product-inner .product-review .review-block .review-info').toggleClass('visible-text');
    $(this).toggleClass('active');
    if($(this).hasClass('active')) {
      $(this).html('Скрыть');
    }else {
      $(this).html('Еще');
    }
  });
  $('.product-inner .product-review .send-review').click(function(){
    $(this).css('display', 'none');
    $('.product-inner .product-review form').slideToggle();
  });
  $('header .mob-bottom-nav .mob-catalog-btn').click(function(){
    $('.mob-menu').removeClass('active');
    $('.catalog-nav').addClass('active');
    $('.mob-search').removeClass('active');
  });
  $('.catalog-nav .mob-top-elem.main-item .back').click(function(){
    $('.catalog-nav').removeClass('active');
  });
  $('header .mob-menu .mob-bottom-nav.item .mob-catalog-btn').click(function(){
    $('.catalog-nav').addClass('menu-opened');
  });
  $('.hamburger').click(function(){
    $('.mob-menu').addClass('active');
    $('.mob-search').removeClass('active');
  });
  $('.mob-menu .close-menu').click(function(){
    $('.mob-menu').removeClass('active');
  });
  $('.mob-search-btn').click(function(){
    $('.mob-search').toggleClass('active');
  });

  $(document).on('click','.catalog-nav.menu-opened .mob-top-elem.main-item .back',function () {
    $('.mob-menu').addClass('active');
    $('.catalog-nav').removeClass('menu-opened');
  });

  window.onscroll = function() {myFunction()};
  var header = document.getElementById("header");
  var sticky = header.offsetTop;

  function myFunction() {
    if (window.pageYOffset > 80) {
      header.classList.add("active");
      $('.content').addClass('active');
    } else {
      header.classList.remove("active");
      $('.content').removeClass('active');
    }
    if (window.pageYOffset > 400) {
      $('.btn-top').addClass('active');
    } else {
      $('.btn-top').removeClass('active');
    }
  }
  $("body").on("click",".anchor", function (event) {
    // исключаем стандартную реакцию браузера
    event.preventDefault();

    // получем идентификатор блока из атрибута href
    var id  = $(this).attr('href'),

    // находим высоту, на которой расположен блок
        top = $(id).offset().top;
     
    // анимируем переход к блоку, время: 800 мс
    $('body,html').animate({scrollTop: top}, 800);
  });

  var myHeigth1 = $('.popular-product.popular-1 .product-block .product-elem:first-child').height();
  $('.popular-product.popular-1 .product-block').css('height', myHeigth1 + 25 + 'px');

  var myHeigth2 = $('.popular-product.popular-2 .product-block .product-elem:first-child').height();
  $('.popular-product.popular-2 .product-block').css('height', myHeigth2 + 25 + 'px');

  if ($(window).width() < 575) {
    var myHeigth1 = $('.popular-product.popular-1 .product-block .product-elem:first-child').height();
    var myHeigth1_1 = $('.popular-product.popular-1 .product-block .product-elem:nth-child(2)').height();
    $('.popular-product.popular-1 .product-block').css('height', (myHeigth1 + myHeigth1_1) + 50 + 'px');

    var myHeigth2 = $('.popular-product.popular-2 .product-block .product-elem:first-child').height();
    var myHeigth2_1 = $('.popular-product.popular-1 .product-block .product-elem:nth-child(2)').height();
    $('.popular-product.popular-2 .product-block').css('height', (myHeigth2 + myHeigth2_1) + 50 + 'px');
  }

  // $('.seo .show-more-btn').click(function(){
  //     var heightBlock = $(this).closest('.container').find('.product-block')[0].scrollHeight;
  //     $(this).toggleClass('hide-block');
  //     if ( $(this).hasClass('hide-block') ){
  //         $(this).closest('.container').find('.product-block').css('height', heightBlock+'px');
  //     }else{
  //         $(this).closest('.container').find('.product-block').css('height', '');
  //     }
  //     return false;
  // });

  $('.popular-product.popular-1 .show-more-btn').click(function(){
      var heightBlock = $(this).closest('.container').find('.product-block')[0].scrollHeight;
      $(this).toggleClass('hide-block');
      if ( $(this).hasClass('hide-block') ){
          $(this).closest('.container').find('.product-block').css('height', heightBlock+'px');
      }else{
        var myHeigth1 = $('.popular-product.popular-1 .product-block .product-elem:first-child').height();
        $(this).closest('.container').find('.product-block').css('height', myHeigth1 + 25 + 'px');
        if ($(window).width() < 575) {
          var myHeigth1 = $('.popular-product.popular-1 .product-block .product-elem:first-child').height();
          var myHeigth1_1 = $('.popular-product.popular-1 .product-block .product-elem:nth-child(2)').height();
          $('.popular-product.popular-1 .product-block').css('height', (myHeigth1 + myHeigth1_1) + 50 + 'px');
          $(this).closest('.container').find('.product-block').css('height', (myHeigth1 + myHeigth1_1) + 50 + 'px');
        }
      }
      return false;
  });

  $('.popular-product.popular-2 .show-more-btn').click(function(){
      var heightBlock = $(this).closest('.container').find('.product-block')[0].scrollHeight;
      $(this).toggleClass('hide-block');
      if ( $(this).hasClass('hide-block') ){
          $(this).closest('.container').find('.product-block').css('height', heightBlock+'px');
      }else{
        var myHeigth2 = $('.popular-product.popular-2 .product-block .product-elem:first-child').height();
        $(this).closest('.container').find('.product-block').css('height', myHeigth2 + 25 + 'px');
        if ($(window).width() < 575) {
          var myHeigth2 = $('.popular-product.popular-2 .product-block .product-elem:first-child').height();
          var myHeigth2_1 = $('.popular-product.popular-1 .product-block .product-elem:nth-child(2)').height();
          $('.popular-product.popular-2 .product-block').css('height', (myHeigth2 + myHeigth2_1) + 50 + 'px');
          $(this).closest('.container').find('.product-block').css('height', (myHeigth2 + myHeigth2_1) + 50 + 'px');
        }
      }
      return false;
  });


    var window_width = $(window).width();
    if (window_width <= 991) {
       $(".mob-sidebar").removeClass('tagline');
    }

    $(".modal-form").submit(function(){ 
    var form = jQuery(this); 
    var error = false; 
    if (!error) { 
      var data = form.serialize();
      jQuery.ajax({ 
         type: 'POST',
         url: '/assets/site/assets_rev/send/modal-form.php', 
         dataType: 'json', 
         data: data, 
         success: function(data){ 
          if (data['error']) { 
            alert(data['error']); 
          } else { 
            $('.info-block').hide();
            $('.modal-form').trigger( 'reset' );
            $(".thx-block").show(); 
          }
         },
         error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status); 
          alert(thrownError); 
         },
         complete: function(data) { 
          form.find('button[type="submit"]').prop('disabled', false); 
         }
    });
    }
    return false; 
  });

  $(".modal-form-dwn").submit(function(){ 
    var form = jQuery(this); 
	var all = jQuery("input[name=price]").val();
	var two = all.split('_'); 
	var lang = two[0];
	var twotp = two[1];
	var idr = two[2];
	if(lang=='ru') {
	var textfile = 'Загрузите прайс-лист по ';
		var textlink = 'ссылке';

	} else {
	var textfile = 'Завантажте прайс-лист за ';
	var textlink = 'посиланням';
	}
	var namefile = 'allprice-'+lang+'.php';
	//alert(lang+twotp+idr+all);
    var error = false; 
    if (!error) { 
      var data = form.serialize();
      jQuery.ajax({ 
         type: 'POST',
         url: '/ajax/telephone', 
         dataType: 'json', 
         data: data, 
                  success: function(data){ 
          if (data['error']) { 
            alert(data['error']); 
          } else { 
		  //alert(data); 
            //window.location.href = '/assets/snippets/price/allprice-'+lang+'.php';
            $('.info-block').hide();
            $('.modal-form-dwn').trigger( 'reset' );
            $(".thx-block").show();
            $('.modal-content .img').hide();
			$('.donwexel').html(textfile+'<a href="/assets/snippets/price/'+namefile+'"><span class="linktitle">'+textlink+'</span></a>');
          }
         },
         error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status); 
          alert(thrownError); 
         },
         complete: function(data) { 
          form.find('button[type="submit"]').prop('disabled', false); 
         }
    });
    }
    return false; 
  });

  $(".modal-form-dwn-cat").submit(function(){ 
    var form = jQuery(this); 
	var all = jQuery("input[name=pricecat]").val();
	var two = all.split('_'); 
	var lang = two[0];
	var twotp = two[1];
	var idr = two[2];
	if(lang=='ru') {
	var textfile = 'Загрузите прайс-лист по ';
		var textlink = 'ссылке';

	} else {
	var textfile = 'Завантажте прайс-лист за ';
	var textlink = 'посиланням';
	}
	var namefile2 = 'allprice-category-'+lang+'.php?idcat='+idr;
    var error = false; 
    if (!error) { 
      var data = form.serialize();
      jQuery.ajax({ 
         type: 'POST',
         url: '/ajax/telephone', 
         dataType: 'json', 
         data: data, 
         success: function(data){ 
          if (data['error']) { 
            alert(data['error']); 
          } else {    
            $('.info-block').hide();
            $('.modal-form-dwn').trigger( 'reset' );
            $(".thx-block").show();
            $('.modal-content .img').hide();
			$('.donwexel2').html(textfile+'<a href="/assets/snippets/price/'+namefile2+'"><span  class="linktitle">'+textlink+'</span></a>');
          }
         },
         error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status); 
          alert(thrownError); 
         },
         complete: function(data) { 
          form.find('button[type="submit"]').prop('disabled', false); 
         }
    });
    }
    return false; 
  });

  $('.modal-backdrop, .close-modal__').click(function(){
    setTimeout(function() { $(".info-block").show();$(".thx-block").hide(); $('.modal-content .img').show(); }, 500);
  });
	jQuery('.about-more').click(function(){
		var text = jQuery(this).text();
		var langd = jQuery(this).data('lang');
		if(langd == 'ua') {
			var pokaz = "Детальніше";
			var notpokaz = "Згорнути";
		} else {
			var pokaz = "Подробнее";
			var notpokaz = "Свернуть";
		}
		//alert(langd);
		jQuery(this).text(text == pokaz ? notpokaz : pokaz);
		if(jQuery(this).parents('.about-vacancy-box').hasClass('active')){
			jQuery(this).parents('.about-vacancy-box').removeClass('active');
		} else{	
			jQuery('.about-vacancies').find('div').removeClass('active');
			jQuery(this).parents('.about-vacancy-box').addClass('active');
		}	
		return false;
	});
	jQuery('.about-more-close').click(function(){
		var langd = jQuery(this).parents('.about-vacancy-box').find('.about-more').data('lang');
		if(langd == 'ua') {
			var pokaz = "Детальніше";
		} else {
			var pokaz = "Подробнее";
		}
		jQuery(this).parents('.about-vacancy-box').find('.about-more').text(pokaz);
		jQuery(this).parents('.about-vacancy-box').removeClass('active');
		jQuery('html, body').animate({
			scrollTop: jQuery(this).parents('.about-vacancy-box').offset().top
		}, 500);
		return false;
	});	
	jQuery('.main-menu li').each(function(){
		if(jQuery(this).find('ul').length>0){
			jQuery(this).addClass('parent');
		}
	});
	jQuery(document).on('click','.main-menu li.parent>a', function(){
		return false;
	});
});


$(window).resize(function() {
  window_width = $(window).width();
  if (window_width <= 991) {
     $(".mob-sidebar").removeClass('tagline');
  } else {
     $(".mob-sidebar").addClass('tagline');
  }
});