<?php
/*
*************************************************************************
	EVO Content Management System and PHP Application Framework ("EVO")
	Managed and maintained by Dmytro Lukianenko and the	EVO community
*************************************************************************
	EVO is an opensource PHP/MySQL content management system and content
	management framework that is flexible, adaptable, supports XHTML/CSS
	layouts, and works with most web browsers.

	EVO is distributed under the GNU General Public License
*************************************************************************

	This file and all related or dependant files distributed with this file
	are considered as a whole to make up EVO.

	EVO is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	EVO is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with EVO (located in "/assets/docs/"); if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1335, USA

	For more information on EVO please visit https://evo.im/
	Github: https://github.com/evolution-cms/evolution/

**************************************************************************
	Based on MODX Evolution CMS and Application Framework
	Copyright 2005 and forever thereafter by Raymond Irving & Ryan Thrash.
	All rights reserved.

	MODX Evolution is originally based on Etomite by Alex Butter
**************************************************************************
*/

/**
 * Initialize Document Parsing
 * -----------------------------
 */
 $url = $_SERVER['REQUEST_URI'];
$par       	= 'page';
$postpar    = 'cat_start';

if(strstr($url, '?'.$postpar.'=')){
 	preg_match_all("/\?".$postpar."=([0-9]++)(.*)/", $url, $res_url);
 	
 	$res_url1 =	(int)$res_url[1][0];
	if (isset ($res_url1) && !empty($res_url1)){
		
		$n_page = $res_url1 / 21 + 1;
		
		if ($n_page){			
			$___url = str_replace( '/'.$res_url[0][0], '/page-'.$n_page, $_SERVER['REQUEST_URI']);
			$___url = str_replace( $res_url[0][0], '/page-'.$n_page, $_SERVER['REQUEST_URI']);
			$___url = str_replace( '/ua/', '/', $___url);
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: ".$___url );
			exit();
		}
	} 	
}

if(strstr($url, '/'.$par.'-')){
	preg_match("/(.*)".$par."-([0-9]++)(.*)/Uis", $url, $output_array);
	if(substr($output_array[1], 0, 1) == '/'){$output_array[1]  = substr($output_array[1], 1);}
    if(isset($output_array[1]) && isset($output_array[3])){
		$n = ( $output_array[2] - 1 ) * 21 ;
		if(strstr($output_array[1].$output_array[3], '?')){
			
//			$_SERVER['REQUEST_URI'] = '/'.$output_array[1].$output_array[3].'&'.$postpar.'='.$n;
			$_SERVER['QUERY_STRING'] = "q=".$output_array[1].$output_array[3].'&'.$postpar.'='.$n;
			$_SERVER["REDIRECT_QUERY_STRING"] = "q=".$output_array[1].$output_array[3].'&'.$postpar.'='.$n;
			
			$_REQUEST["q"] = $output_array[1];
			$_REQUEST["cat_start"] = $n;
			
			$_REQUEST["plerdy_current_add"] = str_replace( '/?'.$postpar.'=0', '/?'.$postpar.'='.$n, $_REQUEST["plerdy_current_add"] );
			$_REQUEST["plerdy_first_add"] = str_replace( '/?'.$postpar.'=0', '/?'.$postpar.'='.$n, $_REQUEST["plerdy_first_add"] );
			
			$_GET["cat_start"] = $n;
			
		}else{
			
//			$_SERVER['REQUEST_URI'] = '/'.$output_array[1].$output_array[3].'?'.$postpar.'='.$n;
			$_SERVER['QUERY_STRING'] = "q=".$output_array[1].'?'.$postpar.'='.$n.$output_array[3];
			$_SERVER["REDIRECT_QUERY_STRING"] = "q=".$output_array[1].'&'.$postpar.'='.$n.$output_array[3];
			
			$_REQUEST["q"] = $output_array[1].$output_array[3];
			$_REQUEST["cat_start"] = $n;
			
			$_REQUEST["plerdy_current_add"] = str_replace( '/?'.$postpar.'=0', '/?'.$postpar.'='.$n, $_REQUEST["plerdy_current_add"] );
			$_REQUEST["plerdy_first_add"] = str_replace( '/?'.$postpar.'=0', '/?'.$postpar.'='.$n, $_REQUEST["plerdy_first_add"] );
			
			$_GET["cat_start"] = $n;
		}
	}
}

 /**
 * 
 * start Tkach web-promo
 * 
 */
	if (strstr($_SERVER['REQUEST_URI'], '?'))
	{
		$url=strtok($_SERVER['REQUEST_URI'], '?');	
	}else{
		$url=$_SERVER['REQUEST_URI'];
	}
	
 	if ( $url == '/index.php' || $url == '/ru'  || $url == '/ua' || $url == '/ua' )
 	{
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: /" );
		exit();
	}	
 	
 	if ( $url == '/ru/katalog/pochatkova-shkola/mebli-dlya-novoi-ukrainskoi-shkoli-nush/shkafy-i-stenki1/shkaf-nush-tip-21/shafa-nush-tip-2' )
 	{
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: /ru/katalog/pochatkova-shkola/mebli-dlya-novoi-ukrainskoi-shkoli-nush/shkafy-i-stenki1/shkaf-nush-tip-21" );
		exit();
	}
	
 	if (mb_ereg( '/ua/', $url ) )
 	{
		if( $url != '/ua/sitemap.xml'){
		$__url = str_replace( '/ua/', '/', $_SERVER['REQUEST_URI']);		
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$__url );
		exit();
		}
	}
 // end

$autoloader = __DIR__.'/vendor/autoload.php';
if (file_exists($autoloader) && is_readable($autoloader)) {
	include_once($autoloader);
}

if(!isset($_SERVER['REQUEST_TIME_FLOAT'])) $_SERVER['REQUEST_TIME_FLOAT'] = microtime(true);

$base_path = str_replace('\\','/',dirname(__FILE__)) . '/';
if(is_file($base_path . 'assets/cache/siteManager.php'))
    include_once($base_path . 'assets/cache/siteManager.php');
if(!defined('MGR_DIR') && is_dir("{$base_path}manager"))
	define('MGR_DIR', 'manager');
if(is_file($base_path . 'assets/cache/siteHostnames.php'))
    include_once($base_path . 'assets/cache/siteHostnames.php');
if(!defined('MODX_SITE_HOSTNAMES'))
	define('MODX_SITE_HOSTNAMES', '');

// get start time
$mstart = memory_get_usage();

// harden it
require_once(dirname(__FILE__).'/'.MGR_DIR.'/includes/protect.inc.php');

// set some settings, and address some IE issues
@ini_set('url_rewriter.tags', '');
@ini_set('session.use_trans_sid', 0);
@ini_set('session.use_only_cookies',1);
session_cache_limiter('');
header('P3P: CP="NOI NID ADMa OUR IND UNI COM NAV"'); // header for weird cookie stuff. Blame IE.
header('Cache-Control: private, must-revalidate');
ob_start();

/**
 *	Filename: index.php
 *	Function: This file loads and executes the parser. *
 */

define('IN_PARSER_MODE', true);
if ( ! defined('IN_MANAGER_MODE')) {
	define('IN_MANAGER_MODE', false);
}
if (!defined('MODX_API_MODE')) {
    define('MODX_API_MODE', false);
}

// get the required includes
if(!isset($database_user) || $database_user=="") {
	$rt = @include_once(dirname(__FILE__).'/'.MGR_DIR.'/includes/config.inc.php');
	// Be sure config.inc.php is there and that it contains some important values
	if(!$rt || !$database_type || !$database_server || !$database_user || !$dbase) {
		readfile('install/not_installed.tpl');
		exit;
	}
}

// start session
startCMSSession();

// initiate a new document parser
if (isset($coreClass) && class_exists($coreClass)) {
	$modx = new $coreClass;
}
if (!isset($modx) || !($modx instanceof \DocumentParser)) {
	include_once(MODX_MANAGER_PATH.'includes/document.parser.class.inc.php');
	$modx = DocumentParser::getInstance();
}

// set some parser options
$modx->minParserPasses = 1; // min number of parser recursive loops or passes
$modx->maxParserPasses = 10; // max number of parser recursive loops or passes
$modx->dumpSQL = false;
$modx->dumpSnippets = false; // feed the parser the execution start time
$modx->dumpPlugins = false;
$modx->tstart = $_SERVER['REQUEST_TIME_FLOAT'];
$modx->mstart = $mstart;

// Debugging mode:
$modx->stopOnNotice = false;

// Don't show PHP errors to the public
if(!isset($_SESSION['mgrValidated']) || !$_SESSION['mgrValidated']) {
    @ini_set("display_errors","0");
}

if(MODX_CLI){
    @set_time_limit(0);
    @ini_set('max_execution_time',0);
}


// execute the parser if index.php was not included
if (!MODX_API_MODE && !MODX_CLI) {
    $modx->executeParser();
}